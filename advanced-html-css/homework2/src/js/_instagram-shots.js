const instagramTitle = document.querySelector(".instagram-shots__title");

window.addEventListener("resize", e =>{
    if (window.innerWidth > 767){
        instagramTitle.textContent = "- Last Instagram Shots"
    }else {
        instagramTitle.textContent = "- Last Instagram Shot"
    }
})
