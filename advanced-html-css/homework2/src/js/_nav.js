const navBurger = document.querySelector(".nav-burger");
const navList = document.querySelector(".page-nav__list");

navBurger.addEventListener("click", event =>{
    document.body.classList.toggle("no-scroll")
    navBurger.classList.toggle("active");
    navList.classList.toggle("active");
});