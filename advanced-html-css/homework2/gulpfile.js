const gulp = require("gulp"),
    sass = require("gulp-sass"),
    concat = require("gulp-concat"),
    clean = require("gulp-clean"),
    browserSync = require("browser-sync").create(),
    autoprefixer = require("gulp-autoprefixer"),
    imagemin = require("gulp-imagemin"),
    uglify = require("gulp-uglify"),
    cleanCss = require("gulp-clean-css");


sass.compiler = require("node-sass");

/*** PATHS ***/
const paths = {
    html: "./index.html",
    src:{
        scss: "./src/scss/**/*.scss",
        js:"./src/js/**/*.js",
        img:"./src/img/**/*"
    },
    build:{
        css: "./dist/*.css",
        js:"./dist/*.js",
        img:"./dist/img",
        self: "./dist/"
    }
}

/*** / PATHS ***/

/*** FUNCTIONS ***/

function cleanBuild (){
    return gulp
        .src(paths.build.self, {allowEmpty: true})
        .pipe(clean());
}

/*** CSS ***/

function buildCSS () {
    return gulp
        .src(paths.src.scss)
        .pipe(sass().on("error", sass.logError))
        .pipe(concat("style.min.css"))
        .pipe(cleanCss())
        .pipe(autoprefixer())
        .pipe(gulp.dest(paths.build.self))
        .pipe(browserSync.stream());
}

/*** / CSS ***/

/*** JS ***/

function buildJS () {
    return gulp
        .src(paths.src.js)
        .pipe(uglify())
        .pipe(concat("script.min.js"))
        .pipe(gulp.dest(paths.build.self))
        .pipe(browserSync.stream());
}

/*** / JS ***/

function buildImage () {
    return gulp
        .src(paths.src.img)
        .pipe(imagemin())
        .pipe(gulp.dest(paths.build.img))
        .pipe(browserSync.stream());
}

function browserSynchronization () {
    browserSync.init({
        server:{
            baseDir: "./"
        }
    });
    gulp.watch(paths.src.scss , buildCSS).on("change",browserSync.reload);
    gulp.watch(paths.src.js , buildJS).on("change",browserSync.reload);
    gulp.watch(paths.html, build).on("change", browserSync.reload);
    gulp.watch(paths.src.img, buildImage).on("change", browserSync.reload);
}


/*** / FUNCTIONS ***/

/*** TASKS ***/

const build = gulp.series(buildCSS, buildJS);

gulp.task("default",gulp.series(cleanBuild, gulp.parallel(buildImage, build), browserSynchronization));
/*** / TASKS ***/

