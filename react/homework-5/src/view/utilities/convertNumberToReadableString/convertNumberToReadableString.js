export function convertNumberToReadableString( number ) {
    if ( typeof number !== "string" && typeof number !== "number") {
        throw "Not valid data type"
    }

    let convertedNumberToString = convertToString(number)
    let fractionalDigits;

    if ( convertedNumberToString.includes(".") ) {
        const stringArr = convertedNumberToString.split(".");
        convertedNumberToString = stringArr[0];
        fractionalDigits = stringArr[1];
        if (fractionalDigits.length === 1) {
            fractionalDigits = `${fractionalDigits}0`
        }
    }

    const formattedString = formatString(convertedNumberToString);

    return fractionalDigits ?
        `${ formattedString }.${ fractionalDigits }` :
        formattedString;
}

function convertToString(data) {
    let string = data.toString();
    string = string.trim()
    const arr = [...string].filter(char => char !== " ");
    return arr.join("")
}

function formatString( string ) {
    const arr = [...string].reverse();
    const formattedArr = arr.map(( item, index ) => {
        if ( index !== 0 && (index % 3 === 0) ) {
            return `${ item } `;
        }
        return item;
    });

    return formattedArr.reverse().join("");
}