import React, { Component } from 'react'
import {Link} from "react-router-dom";

class ErrorBoundary extends Component {
    state = {
        hasError: false
    }

    componentDidCatch(error, errorInfo) {
        this.setState({ hasError: true })
        console.error(error, errorInfo);
    }

    render() {
        const { hasError } = this.state;
        const { children } = this.props;

        if (hasError) {
            return (
                <div>
                    <h1>An error has occurred</h1>
                    <div>
                        <Link to='/'>Go to the Home Page</Link>
                    </div>
                </div>
            )
        }

        return children;
    }
}

export default ErrorBoundary