import React  from "react";
import Goods from "./Goods/Goods.jsx";
import "./Main.scss";

export default function Main() {
    return (
        <main className="main-content">
            <h1 className="main-content__heading">Товары</h1>
            <Goods/>
        </main>
    );
}

