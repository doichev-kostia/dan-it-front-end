import React from "react";
import ProductCard from "../../../../../components/ProductCard/ProductCard.jsx";
import "./GoodsList.scss";
import PropTypes from "prop-types";
import { useSelector } from "react-redux";
import { goodsSelectors } from "../../../../../../redux/features/goods";
import { cartSelectors } from "../../../../../../redux/features/cart";
import { favouritesSelectors } from "../../../../../../redux/features/favourites";

GoodsList.propTypes = {
    classNames: PropTypes.shape({
        cardList: PropTypes.string,
        listItem: PropTypes.string
    })
};

export default function GoodsList(props) {
    const goods = useSelector(goodsSelectors.goods);
    const cart = useSelector(cartSelectors.cart)
    const favourites = useSelector(favouritesSelectors.favourites)

    if (!goods) {
        return <p className="loading">Загрузка ...</p>;
    }

    if ( !goods.length){
        return <p  className="alert no-goods-alert"> Товаров не добавлено</p>
    }


    const allCards = goods.map(card => {
        const isOnCart = cart.some(object => object._id === card._id);
        const isFavourite = favourites.some(object => object._id === card._id);

        return <ProductCard
            key={card._id}
            card={card}
            isOnCart={isOnCart}
            isFavourite={isFavourite}
            classNames={props.classNames}/>;
    });

    return (
        <ul className={props.classNames.cardList}>
            {allCards}
        </ul>
    );
}


