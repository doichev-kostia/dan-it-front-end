import React from "react";
import FavouriteList from "./FavouriteList/FavouriteList.jsx";

function FavouriteSection() {

    const classNames={
        cardList: "favourites__list",
        listItem: "favourites__cell"
    }

    return (
        <section className="favourites">
            <FavouriteList classNames={classNames}/>
        </section>
    );
}

export default FavouriteSection;