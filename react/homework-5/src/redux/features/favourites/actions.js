import types from "./types.js";

const addToFavourites = (id) => ({
    type: types.ADDED_TO_FAVOURITES,
    payload:{ _id: id}
})

const removeFromFavourites = (id) => ({
    type: types.REMOVED_FROM_FAVOURITES,
    payload: id
})

const setFavourites = (data) => ({
    type: types.SET_FAVOURITES,
    payload: data
})


export default {
    addToFavourites,
    removeFromFavourites,
    setFavourites,
}