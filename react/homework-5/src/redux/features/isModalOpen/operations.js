import actions from "./actions.js";

const openModal = () => (dispatch) => {
    dispatch(actions.openModal());
}
const closeModal = () => (dispatch) => {
    dispatch(actions.closeModal());
}

export default {
    openModal,
    closeModal
}