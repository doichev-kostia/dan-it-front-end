import types from "./types.js";



const goodsReducer = ( state = [], action ) => {
    const {type, payload} = action;
    const {GOT_DATA} = types;

    if ( type === GOT_DATA ) {
        return payload;
    } else {
        return state;
    }
};

export default {
    goods: goodsReducer,
};