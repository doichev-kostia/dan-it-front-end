import actions from "./actions.js";
import {getAllData} from "../../../API/serverAPI.js";

const getGoods = () =>  async (dispatch) => {
    const data = await getAllData("goods")
    dispatch(actions.getData(data))
}

const setGoods = (data) => (dispatch) => {
    return dispatch(actions.setData(data));
}


export default {
    getGoods,
    setGoods
}