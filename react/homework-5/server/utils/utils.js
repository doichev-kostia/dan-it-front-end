const ObjectID = require("mongodb").ObjectID;


function getByID( id, collection, res ) {

    collection.findOne({"_id": new ObjectID(id)}, ( err, doc ) => {
        if ( err ) {
            console.log("GET by id", err);
            res.send({"error": "An error has been occurred"});
            return;
        }

        res.send(doc);
    });
}

function getAll( collection, res ) {
    collection.find().toArray(( err, results ) => {
        if ( err ) {
            console.log("GET all", err);
            res.send({"error": "An error has been occurred"});
            return;
        }
        res.send(results);
    });
}

function deleteByID( id, collection, res ) {
    collection.deleteOne({"_id": new ObjectID(id)}, ( err, result ) => {
        if ( err ) {
            console.log("DELETE by id", err);
            res.send({"error": "An error has been occurred"});
            return;
        }

        res.send("Item has been deleted");
    });
}

function updateItem( id, collection, fieldsToUpdate, res ) {
    collection.findOneAndUpdate(
        {_id: new ObjectID(id)},
        {$set: {...fieldsToUpdate}},
        {
            returnOriginal: false
        },
        function ( err, result ) {
            if ( err ) {
                console.log("Update", err);
                res.send({"error": "An error has been occurred"});
                return;
            }

            res.send(result.value);
        }
    );
}

function replaceItem( id, collection, newItem, res ) {
    collection.findOneAndReplace({_id: new ObjectID(id)},
        {...newItem},
        {returnOriginal: false},
        function ( err, result ) {
            if ( err ) {
                console.log("Replace ", err);
                res.send({"error": "An error has been occurred"});
                return;
            }

            console.log("result", result);
            res.send(result);
        });
}

module.exports = {
    getByID,
    getAll,
    deleteByID,
    updateItem,
    replaceItem
};