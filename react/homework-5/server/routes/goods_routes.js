const {
    replaceItem,
    updateItem,
    deleteByID,
    getAll,
    getByID
} = require("./../utils/utils.js");
const ObjectID = require("mongodb").ObjectID;

module.exports = function ( app, collection ) {
    const GOODS_PATH = "/api/goods";
    app.get(`${ GOODS_PATH }`, ( req, res ) => {
        getAll(collection, res);
    });

    app.get(`${ GOODS_PATH }/:id`, async ( req, res ) => {
        const id = req.params.id;

        getByID(id, collection, res);
    });

    app.post(`${ GOODS_PATH }`, ( req, res ) => {
        const {body} = req;

        let product ;

        try {
            product = validateGoodsData(body)
        } catch (err) {
            /**
             * @description {number} 400 -  Bad request. The server could not
             * understand the request due to invalid syntax.
             */
            res.response(400)
            res.send({"error" : err.message})
            return;
        }

        collection.insertOne(product, ( err, result ) => {
            if ( err ) {
                res.send({"error": "An error has been occurred"});
                return console.log("goods POST", err);
            }
            res.send(result.ops);
        });

        res.send(JSON.stringify(product));
    });

    app.put(`${ GOODS_PATH }/:id`, ( req, res ) => {
        const id = req.params.id;
        const item = req.body;

        replaceItem(id, collection, item, res);
    });

    app.patch(`${ GOODS_PATH }/:id`, ( req, res ) => {
        const id = req.params.id;
        const item = req.body;

        updateItem(id, collection, item, res);
    });


    app.delete(`${ GOODS_PATH }/:id`, ( req, res ) => {
        const id = req.params.id;

        deleteByID(id, collection, res);
    });
};

function validateGoodsData( data ) {
    let product = {};

    for ( let [key, value] of Object.entries(data) ) {

        if ( key === "title" && typeof value !== "string" ) {
            throw new Error("Not valid data")
        }

        /**
         * There is an additional condition due to the fact
         * that typeof null === object
         * */
        if ( (key === "price" && typeof value !== "object")
            || (key === "price" && value === null)){
            throw new Error("Not valid data")
        }

        if ( key === "image" && typeof value !== "string"){
            throw new Error("Not valid data")
        }

        product[key] = value
    }

    /**
     * These 3 keys are essential, so we verify that
     * values !== undefined || null
     * */

    const {title, price, image} = product;
    if ( !title || !price || !image ){
        throw new Error("Not valid data")
    }

    return product;

}