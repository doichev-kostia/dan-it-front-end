import Icons from "./Icons.jsx";

export const cartHeaderIcon = <Icons
    name="cart"
    className="header-actions__cart-icon"
    fillColor="#FFFFFF"/>

export const cartGoodsIcon = <Icons
    name="cart"
    className="goods-tile__cart-icon"
    fillColor="#00a046"/>

export const userIcon = <Icons
    name="user"
    className="header-actions__authorization-icon"
    fillColor="#FFFFFF"/>

export const starIcon = <Icons
    name="star"
    className="header-actions__favourites-icon"
    fillColor="#FFD700"/>

export const emptyStarIcon = <Icons
    name="emptyStar"
    fillColor="#FFD700"
    className="header-actions__favourites-icon header-actions__favourites-icon--empty"/>

export const cartChecked = <Icons
    name="cartChecked"
    className="goods-tile__cart-icon goods-tile__cart-icon--checked"
    fillColor="#00a046"/>