import React, {Component} from "react";
import GoodsList from "./GoodsList/GoodsList.jsx";
import "./Goods.scss";
import PropTypes from "prop-types";


class Goods extends Component {
    render() {
        const {props} = this;
        const {goods, modalIDs} = props;

        return (
            <section className="goods">
                <GoodsList
                    modalIDs={modalIDs}
                    goods={goods}
                    toggleModalClosing={props.toggleModalClosing}
                    toggleCartModal={props.toggleCartModal}
                    toggleFavourite={props.toggleFavourite}/>
            </section>
        );
    }
}

Goods.propTypes = {
    modalIDs: PropTypes.object.isRequired,
    goods: PropTypes.shape({
        cart: PropTypes.array,
        favourites: PropTypes.array,
        allGoods: PropTypes.arrayOf(PropTypes.object)
    }),
    toggleModalClosing: PropTypes.func.isRequired,
    toggleCartModal: PropTypes.func.isRequired,
    toggleFavourite: PropTypes.func.isRequired
}



export default Goods;