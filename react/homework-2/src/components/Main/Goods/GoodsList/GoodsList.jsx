import React, {Component} from "react";
import GoodsCard from "../GoodsCard/GoodsCard.jsx";
import "./GoodsList.scss"
import PropTypes from "prop-types";

class GoodsList extends Component {
    render() {

        const {props} = this;
        const {goods} = props;

        if (!goods){
            return null;
        }

         const allCards = goods.allGoods.map(card => {
            return <GoodsCard
                goods={goods}
                key={card.id}
                card={card}
                modalIDs={props.modalIDs}
                toggleModalClosing={props.toggleModalClosing}
                toggleCartModal={props.toggleCartModal}
                toggleFavourite={props.toggleFavourite} />
        })

        return (
            <ul className="goods__list">
                {allCards}
            </ul>
        );
    }
}

GoodsList.propTypes = {
    modalIDs: PropTypes.object.isRequired,
    goods: PropTypes.shape({
        cart: PropTypes.array,
        favourites: PropTypes.array,
        allGoods: PropTypes.arrayOf(PropTypes.object)
    }).isRequired,
    toggleModalClosing: PropTypes.func.isRequired,
    toggleCartModal: PropTypes.func.isRequired,
    toggleFavourite: PropTypes.func.isRequired
}

export default GoodsList;