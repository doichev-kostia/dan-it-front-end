import React, {Component} from "react";
import "./Header.scss"
import Button from "../Common/Button/Button.jsx";
import {userIcon, cartHeaderIcon, starIcon} from "../../utilities/Icons/iconDetails.js"

class Header extends Component {
    render() {

        return (
            <header className="header">
                <div className="wrapper header-layout">
                    <a href="#" className="header__logo">
                        <p className="header__logo-text">Computer Geek</p>
                    </a>
                    <ul className="header-actions">
                       <li className="header-actions__item">
                           <Button className="header-actions__authorization header-actions__button">
                               {userIcon}
                           </Button>
                       </li>
                        <li className="header-actions__item">
                            <Button className="header-actions__cart header-actions__button">
                               {cartHeaderIcon}
                            </Button>
                        </li>
                        <li className="header-actions__item">
                            <Button className="header-actions__favourites header-actions__button">
                                {starIcon}
                            </Button>
                        </li>
                    </ul>
                </div>
            </header>
        );
    }
}

export default Header;