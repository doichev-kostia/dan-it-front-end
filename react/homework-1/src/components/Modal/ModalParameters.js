import Button from "../Button/Button.jsx";

export const ModalParameters = [
    {
        modalID: 0,
        header: "Do you want to delete this file?",
        text: "Once you delete this file, it won’t be possible to undo" +
            " this action. Are you sure you want to delete it?",
        closeButton: true,
        actions:
            [
                <Button
                    key={1}
                    text="OK"
                    backgroundColor={"#B3372BFF"}
                    className="modal-body__btn modal__submit-btn"/>,
                <Button
                    key={2}
                    text="Cancel"
                    backgroundColor={"#B3372BFF"}
                    className="modal-body__btn modal__cancel-btn"/>
            ],
        classNames: {
            modal: "modal",
            modalBody: "modal-body modal-body--red",
            crossButton: "cross-btn modal-body__cross-btn",
            header: "modal-body__header",
            headerWrapper: "modal-body__header-wrapper" +
                " modal-body__header-wrapper--red",
            text: "modal-body__text"
        }
    },
    {
        modalID: 1,
        header: "Do you want to add this item into the cart?",
        text: "This item will be added to your cart",
        closeButton: true,
        actions: [
            <Button
                key={3}
                text="OK"
                backgroundColor={"#23A77BFF"}
                className="modal-body__btn modal__submit-btn"/>,
            <Button
                key={4}
                text="Cancel"
                backgroundColor={"#23A77BFF"}
                className="modal-body__btn modal__cancel-btn"/>
        ],
        classNames: {
            modal: "modal",
            modalBody: "modal-body modal-body--teal",
            crossButton: "cross-btn modal-body__cross-btn",
            header: "modal-body__header",
            headerWrapper: "modal-body__header-wrapper" +
                " modal-body__header-wrapper--teal",
            text: "modal-body__text"
        }
    }
];
