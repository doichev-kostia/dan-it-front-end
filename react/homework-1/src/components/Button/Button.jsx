import React, {Component} from 'react';
import PropTypes from "prop-types";

class Button extends Component {
    render() {
        const {props} = this;
        const {backgroundColor,className, onClick, text } = props;
        const styles = {
            backgroundColor: backgroundColor
        }

        return (
            <button
                className={ className }
                onClick={(e) => {
                    return onClick ? onClick(e) : null
                }}
                style={ styles }>
                { text }
            </button>
        );
    }


}

Button.propTypes = {
    backgroundColor: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func,
    className: PropTypes.string
}

Button.defaultProps = {
    className: "btn"
}

export default Button;