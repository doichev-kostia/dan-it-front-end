import React from "react";
import "./styles/App.scss";
import Modal from "./components/Modal/Modal";
import Button from "./components/Button/Button";
import {ModalParameters} from "./components/Modal/ModalParameters.js";

class App extends React.Component {
    state = {
        openModalID: ""
    };

    render() {
        const {openModalID} = this.state;
        const chosenModal = ModalParameters.find(modal => {
            return modal.modalID.toString() === openModalID.toString();
        });

        if (chosenModal) {
            chosenModal.toggleModalClosing = (id) => this.toggleModalClosing(id);
        }

        return (
            <div>
                <div className="wrapper">
                    <Button
                        className="btn"
                        text="Open first modal"
                        backgroundColor="red"
                        onClick={() => this.toggleModalClosing(0)}/>
                    <Button
                        className="btn"
                        text="Open second modal"
                        backgroundColor="rgb(30,191,138)"
                        onClick={() => this.toggleModalClosing(1)}/>

                    {chosenModal && <Modal {...chosenModal} />}
                </div>
            </div>
        );
    }

    toggleModalClosing(id) {
        const {openModalID} = this.state;
        if (openModalID === id) {
            this.setState({openModalID: ""});
            return;
        }

        this.setState({openModalID: id});
    }


}

export default App;
