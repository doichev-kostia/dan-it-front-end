import React from "react";

export const CartContext = React.createContext();
export const GoodsContext = React.createContext();
export const FavouritesContext = React.createContext();