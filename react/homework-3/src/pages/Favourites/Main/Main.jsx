import React from "react";
import PropTypes from "prop-types";
import FavouriteSection from "./FavouriteSection/FavouriteSection.jsx";

Main.propTypes = {
    addItemToCart:PropTypes.func.isRequired,
    toggleModalClosing:PropTypes.func.isRequired,
    toggleFavourite:PropTypes.func.isRequired
};

function Main(props) {
    return (
        <main className="main-content">
            <h1 className="main-content__heading">Избранное</h1>
            <FavouriteSection {...props}/>
        </main>
    );
}

export default Main;