import React, {useContext} from "react";
import PropTypes from "prop-types";
import ProductCard from "../../../../../components/ProductCard/ProductCard.jsx";
import {CartContext, FavouritesContext, GoodsContext} from "../../../../../context.js";

FavouriteList.propTypes = {
    addItemToCart:PropTypes.func.isRequired,
    toggleModalClosing:PropTypes.func.isRequired,
    toggleFavourite:PropTypes.func.isRequired,
    classNames: PropTypes.shape({
        cardList: PropTypes.string,
        listItem: PropTypes.string
    })
};

export default function FavouriteList(props) {
    const goods = useContext(GoodsContext)
    const cart = useContext(CartContext)
    const favourites = useContext(FavouritesContext)

    if (favourites.length === 0) {
        return <p className="alert no-goods-alert">Товаров не добавлено</p>;
    }

    const favouriteCards = goods.filter(card => {
        return favourites.some(id =>{
            return card.id.toString().trim() === id.toString().trim()
        })

    })

    const list = favouriteCards.map(card => {
        const isOnCart = cart.some(id =>
            id.toString().trim() === card.id.toString().trim());

        return <ProductCard
            key={card.id}
            card={card}
            isOnCart={isOnCart}
            isFavourite={true}
            {...props}/>
    })

    return (
        <ul className={props.classNames.cardList}>
            {list}
        </ul>
    );
}

