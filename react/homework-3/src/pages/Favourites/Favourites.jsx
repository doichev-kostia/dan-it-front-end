import React, {useContext} from "react";
import PropTypes from "prop-types";
import Main from "./Main/Main.jsx";
import {CartContext, FavouritesContext, GoodsContext} from "../../context.js";
import {modalIDs} from "../../components/Common/Modal/ModalParameters.js";
import {addItemToCart, toggleFavourite, toggleModalClosing} from "../../App.js";

Favourites.propTypes = {
    setCart: PropTypes.func.isRequired,
    setFavourites: PropTypes.func.isRequired,
    openModalID: PropTypes.string,
    setModalID: PropTypes.func.isRequired
};

export default function Favourites(props) {
    const {
        openModalID,
        setModalID,
        setCart,
        setFavourites
    } = props;

    const goods = useContext(GoodsContext);
    const cart = useContext(CartContext);
    const favourites = useContext(FavouritesContext);

    return (
        <>
            <div></div>
            <Main
                addItemToCart={(id) => setCart(addItemToCart(id, goods, cart))}
                toggleModalClosing={(e) => setModalID(toggleModalClosing(e, openModalID))}
                toggleFavourite={(id) => setFavourites(toggleFavourite(id, goods, favourites))}/>
        </>
    );
}
