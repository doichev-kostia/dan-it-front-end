import React  from "react";
import Goods from "./Goods/Goods.jsx";
import PropTypes from "prop-types";
import "./Main.scss";


Main.propTypes = {
    toggleModalClosing: PropTypes.func.isRequired,
    toggleFavourite: PropTypes.func.isRequired,
    addItemToCart: PropTypes.func.isRequired
};

export default function Main(props) {
    return (
        <main className="main-content">
            <h1 className="main-content__heading">Товары</h1>
            <Goods {...props}/>
        </main>
    );
}

