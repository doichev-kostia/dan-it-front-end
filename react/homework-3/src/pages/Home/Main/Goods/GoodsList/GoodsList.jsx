import React, {useContext} from "react";
import ProductCard from "../../../../../components/ProductCard/ProductCard.jsx";
import "./GoodsList.scss";
import PropTypes from "prop-types";
import {
    CartContext,
    FavouritesContext,
    GoodsContext
} from "../../../../../context.js";

GoodsList.propTypes = {
    toggleModalClosing: PropTypes.func.isRequired,
    toggleFavourite: PropTypes.func.isRequired,
    addItemToCart: PropTypes.func.isRequired,
    classNames: PropTypes.shape({
        cardList: PropTypes.string,
        listItem: PropTypes.string
    })
};

export default function GoodsList(props) {
    const goods = useContext(GoodsContext)
    const cart = useContext(CartContext)
    const favourites = useContext(FavouritesContext)

    const allCards = goods.map(card => {

        const isOnCart = cart.some(id =>
            id.toString().trim() === card.id.toString().trim());
        const isFavourite = favourites.some(id => id === card.id);

        return <ProductCard
            key={card.id}
            card={card}
            isOnCart={isOnCart}
            isFavourite={isFavourite}
            classNames={props.classNames}
            toggleFavourite={props.toggleFavourite}
            addItemToCart={props.addItemToCart}/>;
    });

    return (
        <ul className={props.classNames.cardList}>
            {allCards}
        </ul>
    );
}


