import React from "react";
import GoodsList from "./GoodsList/GoodsList.jsx";
import "./Goods.scss";
import PropTypes from "prop-types";

Goods.propTypes = {
    toggleModalClosing: PropTypes.func.isRequired,
    toggleFavourite: PropTypes.func.isRequired,
    addItemToCart: PropTypes.func.isRequired
};

export default function Goods(props) {

    const classNames={
        cardList: "goods__list",
        listItem: "goods__cell"
    }

    return (
        <section className="goods">
            <GoodsList classNames={classNames} {...props}/>
        </section>
    );
}



