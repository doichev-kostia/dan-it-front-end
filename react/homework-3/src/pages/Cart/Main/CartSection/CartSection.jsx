import React, {useContext} from "react";
import PropTypes from "prop-types";
import CartList from "./CartList/CartList.jsx";
import "./CartSection.scss"

CartSection.propTypes = {
    setCart: PropTypes.func.isRequired,
    addItemToCart: PropTypes.func.isRequired,
    removeItemFromCart: PropTypes.func.isRequired,
    removeAllSimilarItemsFromCart: PropTypes.func.isRequired
};

function CartSection(props) {

    return (
        <section className="cart">
            <CartList {...props}/>
        </section>
    );
}

export default CartSection;