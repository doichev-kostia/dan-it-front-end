import React, {useContext} from "react";
import PropTypes from "prop-types";
import {CartContext, GoodsContext} from "../../../../../context.js";
import CartItem from "../CartItem/CartItem.jsx";
import "./CartList.scss"

CartList.propTypes = {
    setCart: PropTypes.func.isRequired,
    addItemToCart: PropTypes.func.isRequired,
    removeItemFromCart: PropTypes.func.isRequired,
    removeAllSimilarItemsFromCart: PropTypes.func.isRequired
};

export default function CartList(props) {
    const cart = useContext(CartContext);
    const goods = useContext(GoodsContext);

    if (cart.length === 0) {
        return <p className="alert no-goods-alert">Товаров не добавлено</p>;
    }

    const cartObjects = goods.filter(product => {
        return cart.some(id => {
            return product.id.toString().trim() === id.toString().trim();
        });
    });

    const list = cartObjects.map(item => {
        const quantity = cart.reduce((accumulator, currentValue) =>
            currentValue === item.id ? accumulator + 1 : accumulator, 0)

        return <li className="cart__item">
            <CartItem quantity={quantity} card={item} key={item.id} cart={cart} {...props} {...item}/>
        </li>;
    });

    return (
        <ul className="cart__list">
            {list}
        </ul>
    );
}

