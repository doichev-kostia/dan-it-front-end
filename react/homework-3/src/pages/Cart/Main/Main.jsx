import React from "react";
import PropTypes from "prop-types";
import CartSection from "./CartSection/CartSection.jsx";

Main.propTypes = {
    setCart: PropTypes.func.isRequired,
    addItemToCart: PropTypes.func.isRequired,
    removeItemFromCart: PropTypes.func.isRequired,
    removeAllSimilarItemsFromCart: PropTypes.func.isRequired
};

function Main(props) {
    return (
        <main className="main-content">
            <h1 className="main-content__heading"> Корзина</h1>
            <CartSection {...props}/>
        </main>
    );
}

export default Main;