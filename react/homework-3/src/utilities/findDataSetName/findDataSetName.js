function findDataSetName(event, datasetName, datasetSelector) {
    datasetName = datasetName.toString();
    datasetSelector = datasetSelector.toString();
    const target = event.target;
    const targetChildren = [...target.children];
    const childContainsDataset = targetChildren.find(child =>
        child.dataset[datasetName]);
    const parentContainsDataset = target.closest(datasetSelector);
    const elementContainsDataset = childContainsDataset ||
        parentContainsDataset;

    return elementContainsDataset.dataset[datasetName];
}

export {findDataSetName}