import React from "react";
import PropTypes from "prop-types"
import {Route, Switch} from "react-router-dom";
import Home from "../pages/Home/Home.jsx";
import Cart from "../pages/Cart/Cart.jsx";
import Favourites from "../pages/Favourites/Favourites.jsx";

AppRoutes.propTypes = {
    setCart: PropTypes.func.isRequired,
    setFavourites: PropTypes.func.isRequired,
    openModalID: PropTypes.string,
    setModalID: PropTypes.func.isRequired
}


export default function AppRoutes (props) {
    return (
       <Switch>
           <Route
               exact path="/"
               render={() => <Home {...props}/>}/>
           <Route
               path="/cart"
               render={() => <Cart setCart={props.setCart}/> }/>
           <Route
               path="/favourites"
                render={() => <Favourites {...props}/>}/>
       </Switch>
    );
}
