import React, {useEffect} from "react";
import Button from "../Button/Button.jsx";
import PropTypes from "prop-types";
import "./Modal.scss";
import "../../../styles/common.scss";


Modal.propTypes = {
    header: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    closeButton: PropTypes.bool.isRequired,
    modalID: PropTypes.string.isRequired,
    actions: PropTypes.arrayOf(PropTypes.element).isRequired,
    toggleModalClosing: PropTypes.func.isRequired,
    classNames: PropTypes.shape({
        modal: PropTypes.string,
        modalBody: PropTypes.string,
        crossButton: PropTypes.string,
        header: PropTypes.string,
        headerWrapper: PropTypes.string,
        text: PropTypes.string,
        buttons: PropTypes.string
    })
};

Modal.defaultProps = {
    classNames: {
        modal: "modal",
        modalBody: "modal-body",
        crossButton: "cross-btn modal-body__cross-btn",
        header: "modal-body__header",
        headerWrapper: "modal-body__header-wrapper",
        text: "modal-body__text",
        buttons: "modal-body__buttons"
    }
};

export default function Modal(props) {
    const {modalID, header, text, closeButton, actions, classNames} = props;

    const crossButton = <Button
        text="X"
        backgroundColor="transparent"
        className={classNames.crossButton}
        onClick={(e) => props.toggleModalClosing(e)}/>;


    useEffect(() => {
        document.body.classList.add("lock-screen");
        return () => {
            document.body.classList.remove("lock-screen");
        };
    }, []);

    return (
        <div onClick={(e) => handleModalClosing(e, props.toggleModalClosing)}
             className={classNames.modal}>
            <div data-modal-id={modalID} className={classNames.modalBody}>
                <div className={classNames.headerWrapper}>
                    {closeButton && crossButton}
                    <p className={classNames.header}>{header}</p>
                </div>
                <p className={classNames.text}>{text}</p>
                <div className={classNames.buttons}>
                    {actions}
                </div>
            </div>
        </div>
    );
}

function handleModalClosing(e, toggleModalClosing) {
    const target = e.target;
    const targetTagName = e.target.tagName.toLowerCase();
    if (target === e.currentTarget || targetTagName === "button") {
        toggleModalClosing(e);
    }
}


