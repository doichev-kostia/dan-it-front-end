import React, {useContext, useState} from "react";
import Button from "../Common/Button/Button.jsx";
import {modalIDs} from "../Common/Modal/ModalParameters.js";
import {
    starIcon,
    emptyStarIcon,
    cartGoodsIcon,
    cartChecked
} from "../../utilities/Icons/iconDetails.js";
import "./ProductCard.scss";
import PropTypes from "prop-types";
import Modal from "../Common/Modal/Modal.jsx";
import {useHistory} from "react-router-dom";
import {CartContext} from "../../context.js";
import {getChosenModal, toggleCartModal} from "../../App.js";


ProductCard.propTypes = {
    card: PropTypes.shape({
        title: PropTypes.string,
        price: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        image: PropTypes.string,
        id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        color: PropTypes.oneOfType([PropTypes.string, PropTypes.arrayOf(PropTypes.string)])
    }),
    isOnCart: PropTypes.bool.isRequired,
    isFavourite: PropTypes.bool,
    classNames: PropTypes.shape({
       listItem: PropTypes.string
    }),
    toggleFavourite: PropTypes.func.isRequired,
    addItemToCart: PropTypes.func.isRequired
};


export default function ProductCard(props) {
    const {card, isOnCart, isFavourite} = props;
    const cart = useContext(CartContext);
    const [openModalID, setOpenModalID] = useState("");
    const history = useHistory();

    if (!card) return null;


    const {title, price, image, id} = card;
    const chosenModal = getChosenModal(openModalID, setOpenModalID);

    return (
        <li className={props.classNames.listItem}>
            <div className="goods-tile">
                <div className="goods-tile__inner">
                    <a href="#" className="goods-tile__picture">
                        <img src={image} alt={title}/>
                    </a>
                    <div className="goods-tile__actions">
                        <Button type="button"
                                onClick={() => {
                                    props.toggleFavourite(id);
                                }}
                                className="goods-tile__favourite-btn"
                                children={isFavourite ?
                                    starIcon : emptyStarIcon}/>
                    </div>
                    <div className="goods-tile__colors">
                    </div>
                    <a href="#" className="goods-tile__title">{title}</a>
                    <div className="goods-tile__prices">
                        <p className="goods-tile__price">
                                <span
                                    className="goods-tile__price-value">{price.toString()}</span>
                            <span
                                className="goods-tile__price-currency">₴</span>
                        </p>
                        <Button
                            className="goods-tile__cart-button"
                            data-modal-id={modalIDs.cart}
                            data-card-id={card.id}
                            children={isOnCart ?
                                cartChecked : cartGoodsIcon}
                            onClick={(e) =>
                                cartButtonBehaviour(e, history, isOnCart, cart, props.addItemToCart, setOpenModalID)}/>
                    </div>
                </div>
            </div>
            {chosenModal
            &&
            <Modal {...chosenModal}/>}
        </li>
    );
}

function cartButtonBehaviour(event, history, isOnCart, cart, addItemToCart, setOpenModalID) {
    if (isOnCart) {
        history.push("/cart");
        return;
    }

    toggleCartModal(event, cart, addItemToCart, setOpenModalID);
}

