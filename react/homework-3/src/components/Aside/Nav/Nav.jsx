import React, {Component} from "react";
import "./Nav.scss"

export default function Nav (props){
        return (
            <nav className="nav">
                <ul className="nav__list">
                    <li className="nav__item">
                        <a className="nav__link link" href="#">Ноутбуки</a>
                    </li>
                    <li className="nav__item">
                        <a className="nav__link link" href="#">Компьютеры</a>
                    </li>
                    <li className="nav__item">
                        <a className="nav__link link" href="#">Смартфоны</a>
                    </li>
                    <li className="nav__item">
                        <a className="nav__link link" href="#">ТВ и электроника</a>
                    </li>
                    <li className="nav__item">
                        <a className="nav__link link" href="#">Товары для геймеров</a>
                    </li>
                    <li className="nav__item">
                        <a className="nav__link link" href="#">Аксессуары</a>
                    </li>
                </ul>
            </nav>
        );
}
