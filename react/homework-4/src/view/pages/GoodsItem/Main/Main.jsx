import React, { useEffect } from "react";
import { useRouteMatch } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
    goodsOperations,
    goodsSelectors
} from "../../../../redux/features/goods";
import {
    favouritesSelectors
} from "../../../../redux/features/favourites";
import ExtendedProductCard from "./ExtendedProductCard/ExtendedProductCard.jsx";

function Main( ) {
    const dispatch = useDispatch()
    const favourites = useSelector(favouritesSelectors.favourites)
    const match = useRouteMatch();
    const id = match.params.id;
    const goods = useSelector(goodsSelectors.goods);
    const isFavourite = favourites.some(object => object._id === id);

    useEffect(() => {
        dispatch(goodsOperations.getGoods());
    }, []);



    if (!goods || !goods.length) {
        return <p className="loading">Загрузка ...</p>;
    }

    const chosenItem = goods.find(object => object._id === id);

    if (!chosenItem) {
        return <p  className="alert no-goods-alert"> Такого товара не существует </p>
    }

    const {title} = chosenItem;

    return (
        <main className="main-content">
            <section className="product">
                <h1 className="product__heading main-content__heading">
                    { title }
                </h1>
                <ExtendedProductCard chosenItem={chosenItem} isFavourite={isFavourite}/>
            </section>
        </main>
    );
}

export default Main;