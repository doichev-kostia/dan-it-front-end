import React from "react";
import CartItem from "../CartItem/CartItem.jsx";
import "./CartList.scss"
import { useSelector } from "react-redux";
import { cartSelectors } from "../../../../../../redux/features/cart";
import { goodsSelectors } from "../../../../../../redux/features/goods";

export default function CartList(props) {
    const cart = useSelector(cartSelectors.cart);
    const goods = useSelector(goodsSelectors.goods)

    if ( !cart ){
        return <p className="loading"> Загрузка ...</p>
    }

    if (!cart.length) {
        return <p className="alert no-goods-alert">Товаров не добавлено</p>;
    }

    const cartObjects = goods.filter(product => {
        return cart.some(object => product._id === object._id);
    });

    const list = cartObjects.map(item => {
        let quantity = cart.find(object => object._id === item._id).quantity;

        return <li className="cart__item">
            <CartItem key={item._id} quantity={quantity} card={item} {...item}/>
        </li>;
    });

    return (
        <ul className="cart__list">
            {list}
        </ul>
    );
}

