import React from "react";
import CartList from "./CartList/CartList.jsx";
import "./CartSection.scss"

function CartSection() {

    return (
        <section className="cart">
            <CartList />
        </section>
    );
}

export default CartSection;