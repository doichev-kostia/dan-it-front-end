function* keyGenerator(){
    let key = 0;
    while (true)
        yield key++
}

const key = keyGenerator()

export const uniqueKey = () => key.next().value;

function* idGenerator(){
    let id = 0;
    while (true)
        yield id++
}

const id = idGenerator()

export const newId = () => id.next().value;