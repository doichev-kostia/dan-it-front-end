export function argumentsValidation(className, fill, functionName){
    if (typeof className !== "string" || Array.isArray(className)){
        throw new Error(`Not valid class name in ${functionName} function. Input:  ${className}`);
    }
    if (typeof fill !== "string") {
        throw new Error(`Not valid class name in ${functionName} function. Input:  ${fill}`);
    }

    if (Array.isArray(className)){
        className = className.join(" ");
    }

    return {className}

}