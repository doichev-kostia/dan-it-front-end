import React, {useEffect} from "react";
import {modalParameters} from "./modalParameters.js";
import Button from "../Button/Button.jsx";
import PropTypes from "prop-types";
import "./Modal.scss";
import "../../styles/common.scss";
import { useDispatch } from "react-redux";
import { isModalOpenOperations } from "../../../redux/features/isModalOpen";


Modal.propTypes = {
    header: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    closeButton: PropTypes.bool.isRequired,
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    actions: PropTypes.arrayOf(PropTypes.element).isRequired,
    classNames: PropTypes.shape({
        modal: PropTypes.string,
        modalBody: PropTypes.string,
        crossButton: PropTypes.string,
        header: PropTypes.string,
        headerWrapper: PropTypes.string,
        text: PropTypes.string,
        buttons: PropTypes.string
    })
};

Modal.defaultProps = {
    classNames: {
        modal: "modal",
        modalBody: "modal-body",
        crossButton: "cross-btn modal-body__cross-btn",
        header: "modal-body__header",
        headerWrapper: "modal-body__header-wrapper",
        text: "modal-body__text",
        buttons: "modal-body__buttons"
    }
};

export default function Modal(props) {
    const {id, header, text, closeButton, actions, classNames} = props;
    const dispatch = useDispatch()

    const crossButton = <Button
        text="X"
        backgroundColor="transparent"
        className={classNames.crossButton}
        onClick={(e) => handleModalClosing(e, dispatch)}/>;


    useEffect(() => {
        document.body.classList.add("lock-screen");
        return () => {
            document.body.classList.remove("lock-screen");
        };
    }, []);

    return (
        <div onClick={(e) => handleModalClosing(e, dispatch)}
             className={classNames.modal}>
            <div data-modal-id={id} className={classNames.modalBody}>
                <div className={classNames.headerWrapper}>
                    {closeButton && crossButton}
                    <p className={classNames.header}>{header}</p>
                </div>
                <p className={classNames.text}>{text}</p>
                <div className={classNames.buttons}>
                    {actions}
                </div>
            </div>
        </div>
    );
}

function handleModalClosing(e, dispatch) {
    const target = e.target;
    const targetTagName = e.target.tagName.toLowerCase();
    if (target === e.currentTarget || targetTagName === "button") {
       dispatch(isModalOpenOperations.closeModal())
    }
}

export function generateModalData (id) {
    return modalParameters.find(modal=>
        modal.id.toString().trim() === id.toString().trim())
}

