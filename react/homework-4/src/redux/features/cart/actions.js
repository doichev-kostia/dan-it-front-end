import types from "./types.js";

const addToCart = ( id, quantity ) => ({
    type: types.ADDED_TO_CART,
    payload: {
        _id: id,
        quantity: quantity
    }
});

const setAmount = (updatedCart) => ({
    type: types.SET_AMOUNT,
    payload: updatedCart
})

const increaseAmount = (updatedCart) => ({
    type: types.INCREASED_AMOUNT,
    payload: updatedCart
})

const decreaseAmount = (updatedCart) => ({
    type: types.INCREASED_AMOUNT,
    payload: updatedCart
})

const removeFromCart = ( id ) => ({
    type: types.REMOVED_FROM_CART,
    payload: id
});

const getCart = ( data ) => ({
    type: types.SET_CART,
    payload: data
});



export default {
    setAmount,
    increaseAmount,
    decreaseAmount,
    addToCart,
    removeFromCart,
    getCart,
};