const goodsRoutes = require("./goods_routes.js");
const cartRoutes = require("./cart_routes.js");
const favouritesRoutes = require("./favourites_routes.js")

module.exports = function ( app, client ) {
    const db = client.db("react-homeworks");
    const goodsCollection = db.collection("goods");
    const cartCollection = db.collection("cart");
    const favouritesCollection = db.collection("favourites")

    goodsRoutes(app, goodsCollection);
    cartRoutes(app, cartCollection)
    favouritesRoutes(app, favouritesCollection)
}