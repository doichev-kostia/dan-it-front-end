const {replaceItem, updateItem, deleteByID, getAll, getByID} = require("./../utils/utils.js")
const ObjectID = require("mongodb").ObjectID;

module.exports = function ( app, collection ) {
    const FAVOURITES_PATH = "/api/favourites";

    app.get(`${FAVOURITES_PATH}`, (req, res) => {
        getAll(collection, res);
    })

    app.get(`${FAVOURITES_PATH}/:id`, (req, res) => {
        const id = req.params.id;
        getByID(id, collection, res)
    })

    app.post(`${FAVOURITES_PATH}`, (req, res) => {
        const {body} = req;

        collection.insertOne({"_id": new ObjectID(body._id)}, (err, result) => {
            if ( err ) {
                res.send({"error" : "An error has been occurred"})
                return console.log("favourites POST",err)
            }

            res.send(result.ops)
        })
    })

    app.put(`${FAVOURITES_PATH}/:id`, (req, res) => {
        const id = req.params.id;
        const item = req.body

        replaceItem(id, collection, item, res)
    })

    app.patch(`${FAVOURITES_PATH}/:id`, (req, res) => {
        const id = req.params.id;
        const item = req.body;

        updateItem(id, collection, item, res)
    })

    app.delete(`${FAVOURITES_PATH}/:id`, (req, res) => {
        const id = req.params.id;

        deleteByID(id, collection, res)
    })


}