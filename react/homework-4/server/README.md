# Server API

There are several paths:

* ```javascript
    "/api/goods" 
   ```
* ```javascript
    "/api/cart" 
   ```  
* ```javascript
    "/api/favourites" 
   ```    

#  

#### In favourites collection it is required to send object:

```javascript
    {
        _id : "id"
    }
   ```

From the server you will get a response like:

```javascript
   {
         _id : "id"
   }
   ```

#

#### In cart collection it is required to send object :

```javascript
     {
        _id : "id",
        quantity : numberOfsuchItemsOnCart    
    }
   ```

From the server you will get a response like:

```javascript
  {
        _id : "id",
        quantity : numberOfsuchItemsOnCart
    }
   ```

#


## Methods

### Get all:

```javascript
fetch("/api/goods")
    .then(response => response.json())
    .then(data => console.log(data))
```

After this request server will send you all the data that's stored in the collection.

Goods data :

Array with objects

```javascript
[
    {
        title: "Ноутбук Apple MacBook Pro 16 512GB 2019",
        price: {
            UAH: {
                value: "100 000",
                symbol: "₴",
                oldPrice: "120 000"
            },
            USD: {
                value: "3 500",
                symbol: "$",
                oldPrice: "3 600"
            },
        },
        image: "./img/goods/macbook-pro-16.jpeg",
        characteristic: "Some text",
        colors: [
            {
                title: "Black",
                rgb: "rgb(0,0,0)",
                hex: "#000000"
            },
            {
                title: "White",
                rgb: "rgb(255,255,255)",
                hex: "#FFFFFF"
            }
        ]
    }
]
   ```  

Cart data :

Array with objects

```javascript
[
    {
        _id: "some id",
        quantity: number
    }
]
   ```  

Favourites data:

Array with objects

```javascript
[
    {
        _id: "some id",
    }
]
   ```  

### Get by id

```javascript
fetch("/api/goods/id")
    .then(response => response.json())
    .then(data => console.log(data))
```

The server will return an object with requested _id

### Post

```javascript
fetch("/api/goods", {
    method: "POST",
    headers: {
        'Content-Type': 'application/json;charset=utf-8'
    },
    body: JSON.stringify(dataToBeSent)

})
    .then(response => response.json())
    .then(data => console.log(data))
```
#

####Goods data should look like: 
```javascript
 data = {
        title: "Ноутбук Apple MacBook Pro 16 512GB 2019", 
        price: {
                UAH: {
                    value: "100 000",
                    symbol: "₴",
                    oldPrice: "120 000"
                },
                USD: {
                    value: "3 500",
                    symbol: "$",
                    oldPrice: "3 600"
                },
            },
        image: "./img/goods/macbook-pro-16.jpeg",
        characteristic: "Some text",    
        colors: [
            {
                title: "Black",
                rgb: "rgb(0,0,0)",
                hex: "#000000"
            },
            {
                title: "White",
                rgb: "rgb(255,255,255)",
                hex: "#FFFFFF"
            }
         ]   
}
```

In `price` object, each field is a currency code

#

#### Cart data should look like:

```javascript
data = {
    _id: "goods card _id",
    quantity: "number of similar items on cart"
}
```

#

####Favourites data should look like: 

```javascript
data = {
    _id: "goods card _id"
}
```


The data that will come from server - an object that was sent with _id field, in case this field has not been declared

### PATCH


It is important to pass the object without id and it can contain fields that has changed (or been added) as well as a new version of the object.
It is essential to realize that this method to not overwrite the data, it only updates the object that was found by id

In the example below, we want to change a `price` field, so we can pass only this field in fetch body.

```javascript
dataToChange = {
    title: "Ноутбук Apple MacBook Pro 16 512GB 2019",
    price: {
        UAH: {
            value: "100 000",
            symbol: "₴",
            oldPrice: "120 000"
        },
        USD: {
            value: "3 500",
            symbol: "$",
            oldPrice: "3 600"
        },
    },
    image: "./img/goods/macbook-pro-16.jpeg",
    characteristic: "Some text",
    colors: [
        {
            title: "Black",
            rgb: "rgb(0,0,0)",
            hex: "#000000"
        },
        {
            title: "White",
            rgb: "rgb(255,255,255)",
            hex: "#FFFFFF"
        }
    ]
}
```

```javascript
fetch("/api/goods/id", {
    method: "PATCH",
    headers: {
        'Content-Type': 'application/json;charset=utf-8'
    },
    body: {
        price: {
            UAH: {
                value: "95 000",
                symbol: "₴",
                oldPrice: "120 000"
            },
            USD: {
                value: "3 350",
                symbol: "$",
                oldPrice: "3 600"
            }
        },
    }
})
    .then(response => response.json())
    .then(data => console.log(data))
```

So, our response is going to be : 
```javascript
 {
    title: "Ноутбук Apple MacBook Pro 16 512GB 2019",
    price: {
        UAH: {
            value: "95 000",
            symbol: "₴",
            oldPrice: "120 000"
        },
        USD: {
            value: "3 350",
            symbol: "$",
            oldPrice: "3 600"
        }
    },
    image: "./img/goods/macbook-pro-16.jpeg",
    characteristic: "Some text",
    colors: [
        {
            title: "Black",
            rgb: "rgb(0,0,0)",
            hex: "#000000"
        },
        {
            title: "White",
            rgb: "rgb(255,255,255)",
            hex: "#FFFFFF"
        }
    ]
}
```

### PUT

This method replaces an item that was found by id with a new data. It's important not to pass object id in the request  body.

In the example below we are going to replace `image` field with `picture` one

```javascript
data = {
    title: "Ноутбук Apple MacBook Pro 16 512GB 2019",
    price: {
        UAH: {
            value: "100 000",
            symbol: "₴",
            oldPrice: "120 000"
        },
        USD: {
            value: "3 500",
            symbol: "$",
            oldPrice: "3 600"
        },
    },
    image: "./img/goods/macbook-pro-16.jpeg",
    characteristic: "Some text",
    colors: [
        {
            title: "Black",
            rgb: "rgb(0,0,0)",
            hex: "#000000"
        },
        {
            title: "White",
            rgb: "rgb(255,255,255)",
            hex: "#FFFFFF"
        }
    ]
}
```


```javascript
fetch("/api/goods/id", {
    method: "PUT",
    headers: {
        'Content-Type': 'application/json;charset=utf-8'
    },
    body: {
            title: "Ноутбук Apple MacBook Pro 16 512GB 2019",
            price: {
                UAH: {
                    value: "100 000",
                    symbol: "₴",
                    oldPrice: "120 000"
                },
                USD: {
                    value: "3 500",
                    symbol: "$",
                    oldPrice: "3 600"
                },
            },
            picture: "./img/goods/macbook-pro-16.jpeg",
            characteristic: "Some text",
            colors: [
                {
                    title: "Black",
                    rgb: "rgb(0,0,0)",
                    hex: "#000000"
                },
                {
                    title: "White",
                    rgb: "rgb(255,255,255)",
                    hex: "#FFFFFF"
                }
            ]
    }
})
```

As a response we are going to receive 

```javascript
   {
    title: "Ноутбук Apple MacBook Pro 16 512GB 2019",
        price: {
        UAH: {
            value: "100 000",
                symbol: "₴",
                oldPrice: "120 000"
        },
        USD: {
            value: "3 500",
                symbol: "$",
                oldPrice: "3 600"
        },
    },
    picture: "./img/goods/macbook-pro-16.jpeg",
        characteristic: "Some text",
        colors: [
        {
            title: "Black",
            rgb: "rgb(0,0,0)",
            hex: "#000000"
        },
        {
            title: "White",
            rgb: "rgb(255,255,255)",
            hex: "#FFFFFF"
        }
    ]
}
```

### Delete

```javascript
fetch("/api/goods/id", {
    method: "DELETE",
    headers: {
        'Content-Type': 'application/json;charset=utf-8'
    }
})
    .then(response => response.json())
    .then(data => console.log(data))
```

After deleting some data you will get a string that confirms that this data has been deleted