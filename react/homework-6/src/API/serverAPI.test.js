import {
    getAllData,
    patchData,
    deleteData,
    postData,
    getDataByID,
    putData
} from "./serverAPI";

const SERVER_DATA = [
    {
        _id: 1
    },
    {
        _id: 2
    }
]

/**
 * it's important to realize that these tests do not actually show how the functions work,
 * for more information go to serverApi.js or readme.md in server folder
 * */

describe("server API", () => {
    beforeEach(() => {
        global.fetch = jest.fn();
        global.fetch.mockReturnValue({
            json: () => SERVER_DATA,
            ok: true
        });
    })

    describe("getAllData", () => {
        test("Smoke", () => {
            getAllData("test")
            expect(global.fetch).toHaveBeenCalled();
        });

        test("if it returns any value", async () => {
            expect(await getAllData("test")).toStrictEqual(SERVER_DATA);
        });

        test("if it throws an Error when the result is !ok",  async () => {
            global.fetch.mockReturnValue({
                json: () => SERVER_DATA,
                ok: false,
                status: 404,
                statusText: "ERROR"
            });

            expect(async () => await getAllData("test")).toThrow();
        })
    });

    describe("getDataByID", () => {
        test("Smoke", () => {
            getDataByID("test", "1")
            expect(global.fetch).toHaveBeenCalled();
        });

        test("if it returns any value", async () => {
            expect(await getDataByID("test", "1")).toStrictEqual(SERVER_DATA);
        });
    });

    describe("postData", () => {
        test("Smoke", () => {
            postData("test", [{data: "some data"}]);
            expect(global.fetch).toHaveBeenCalled();
        });

        test("if it returns any value", async () => {
            expect(await postData("test", [{data: "some data"}])).toStrictEqual(SERVER_DATA);
        });
    });

    describe("putData", () => {
        test("Smoke", () => {
            putData("test", "1",[{data: "some data"}]);
            expect(global.fetch).toHaveBeenCalled();
        });

        test("if it returns any value", async () => {
            expect(await putData("test", "1",[{data: "some data"}])).toStrictEqual(SERVER_DATA);
        });
    });

    describe("patchData", () => {
        test("Smoke", () => {
            patchData("test", "1",[{data: "some data"}]);
            expect(global.fetch).toHaveBeenCalled();
        });

        test("if it returns any value", async () => {
            expect(await patchData("test", "1",[{data: "some data"}])).toStrictEqual(SERVER_DATA);
        });
    })

    describe("deleteData", () => {
        test("Smoke", () => {
            deleteData("test", "1");
            expect(global.fetch).toHaveBeenCalled();
        });
    })


})
