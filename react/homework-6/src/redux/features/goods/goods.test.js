import actions from "./actions";
import types from "./types";
import reducers from "./reducers";
import selectors from "./selectors";
import {goodsOperations} from "./index";


const {
    getData
} = actions;

const {
    GOT_DATA
} = types;

describe("reducers", () => {
    const reducer = reducers.goods;

    test("smoke", () => {
        const result = reducer(undefined, {type: "@@INIT"});
        expect(result).toStrictEqual([]);
    });

    test("if it returns correct state in case GOT_DATA action is passed", () => {
        const action = getData([{_id: 2}]);
        const result = reducer([], action);
        expect(result).toEqual(action.payload)
    });
});

describe("selectors ", () => {
    const goods = selectors.goods;
    test("smoke", () => {
        const state = {
            goods: []
        };

        const result = goods(state);
        expect(result).toEqual(state.goods);
    })
});

describe("actions", () => {
    describe("getData", () => {
        test("smoke", () => {
            expect(typeof getData([{_id: 1}])).toBe("object");
        });

        test("return value if no arguments is passed", () => {
            const output = {
                type: GOT_DATA,
                payload: undefined
            }
           expect(getData()).toStrictEqual(output);
        });

        test("return value if data argument is passed", () => {
            const output = {
                type: GOT_DATA,
                payload: [{_id: 1}]
            }
           expect(getData([{_id: 1}])).toStrictEqual(output);
        });
    })
});

describe("operations", () => {
    describe("getGoods", () => {
        test("smoke", () => {
            expect(typeof goodsOperations.getGoods()).toBe("function")
        });
    });

    describe("setGoods", () => {
        test("smoke", () => {
            expect(typeof goodsOperations.setGoods([])).toBe("function")
        })
    })
});