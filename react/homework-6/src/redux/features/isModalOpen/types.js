const OPENED = "isModalOpen/opened";
const CLOSED = "isModalOpen/closed";

export default {
    OPENED,
    CLOSED
}