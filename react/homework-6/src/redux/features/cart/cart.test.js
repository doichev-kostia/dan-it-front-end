import actions from "./actions";
import types from "./types";
import reducers from "./reducers";
import selectors from "./selectors";

const {
    setAmount,
    increaseAmount,
    decreaseAmount,
    addToCart,
    removeFromCart,
    getCart,
    removeAll
} = actions;

const {
    GOT_CART,
    ADDED_TO_CART,
    SET_AMOUNT,
    DECREASED_AMOUNT,
    INCREASED_AMOUNT,
    REMOVED_FROM_CART,
    REMOVED_ALL
} = types;

describe("cart reducers", () => {
    const reducer = reducers.cart;
    test("smoke", () => {
        const result = reducer(undefined, {type: "@@INIT"});
        expect(result).toEqual([]);
    });

    test("if it returns correct state in case ADDED_TO_CART action is passed", () => {
        const action = addToCart(1, 1);
        const result = reducer([], action);
        expect(result).toEqual([action.payload]);
    });

    test("if it returns correct state in case SET_AMOUNT action is passed", () => {
        const action = setAmount([]);
        const result = reducer([], action);
        expect(result).toEqual(action.payload);
    });

    test("if it returns correct state in case INCREASED_AMOUNT action is passed", () => {
        const action = increaseAmount([]);
        const result = reducer([], action);
        expect(result).toEqual(action.payload);
    });

    test("if it returns correct state in case DECREASED_AMOUNT action is passed", () => {
        const action = decreaseAmount([]);
        const result = reducer([], action);
        expect(result).toEqual(action.payload);
    });

    test("if it returns correct state in case GOT_CART action is passed", () => {
        const action = getCart([]);
        const result = reducer([], action);
        expect(result).toEqual(action.payload);
    });

    test("if it returns correct state in case REMOVED_FROM_CART action is passed", () => {
        const action = removeFromCart(1);
        const prevState = [
            {
                _id: 1,
            },
            {
                _id: 2
            }];
        const result = reducer(prevState, action);
        expect(result).toEqual([{_id: 2}]);
    });

    test("if it returns correct state in case REMOVED_ALL action is passed", () => {
        const action = removeAll();
        const prevState = [
            {
                _id: 1,
            },
            {
                _id: 2
            }];
        const result = reducer(prevState, action);
        expect(result).toEqual([]);
    });

});

describe("cart actions", () => {

    describe("getCart", () => {
        test("getCart smoke", () => {
            expect(typeof getCart()).toBe("object");
        });

        test("getCart return value if no arguments passed", () => {
            const output = {
                type: GOT_CART,
                payload: undefined
            };
            expect(getCart(undefined)).toStrictEqual(output);
        });

        test("getCart return value if all arguments are passed", () => {
            const input = [];
            const output = {
                type: GOT_CART,
                payload: input
            };

            expect(getCart(input)).toStrictEqual(output);
        });
    })

    describe("setAmount", () => {
        test("setAmount smoke", () => {
            expect(typeof setAmount()).toBe("object");
        });

        test("setAmount return value if no arguments passed", () => {
            const output = {
                type: SET_AMOUNT,
                payload: undefined
            };
            expect(setAmount(undefined)).toStrictEqual(output);
        });

        test("setAmount return value if all arguments are passed", () => {
            const input = [];
            const output = {
                type: SET_AMOUNT,
                payload: input
            };

            expect(setAmount(input)).toStrictEqual(output);
        });
    })

    describe("increaseAmount", () => {
        test("increaseAmount smoke", () => {
            expect(typeof increaseAmount()).toBe("object");
        })

        test("increaseAmount return value if no arguments passed", () => {
            const output = {
                type: INCREASED_AMOUNT,
                payload: undefined
            };
            expect(increaseAmount(undefined)).toStrictEqual(output);
        });

        test("increaseAmount return value if all arguments are passed", () => {
            const input = [];
            const output = {
                type: INCREASED_AMOUNT,
                payload: input
            };

            expect(increaseAmount(input)).toStrictEqual(output);
        });
    })

    describe("decreaseAmount", () => {
        test("decreaseAmount smoke", () => {
            expect(typeof decreaseAmount()).toBe("object");
        })

        test("decreaseAmount return value if no arguments passed", () => {
            const output = {
                type: DECREASED_AMOUNT,
                payload: undefined
            };
            expect(decreaseAmount(undefined)).toStrictEqual(output);
        });

        test("decreaseAmount return value if all arguments are passed", () => {
            const input = [];
            const output = {
                type: DECREASED_AMOUNT,
                payload: input
            };

            expect(decreaseAmount(input)).toStrictEqual(output);
        });
    });

    describe("addToCart", () => {
        test("addToCart smoke", () => {
            expect(typeof addToCart()).toBe("object");
        })

        test("addToCart return value if no arguments are passed", () => {
            const output = {
                type: ADDED_TO_CART,
                payload: {
                    _id: undefined,
                    quantity: undefined
                }
            };
            expect(addToCart(undefined, undefined)).toStrictEqual(output);
        });

        test("addToCart return value if only id argument is passed", () => {
            const input = {
                id: 1
            };

            const output = {
                type: ADDED_TO_CART,
                payload: {
                    _id: input.id,
                    quantity: undefined
                }
            };

            expect(addToCart(input.id, undefined)).toStrictEqual(output);
        });

        test("addToCart return value if only quantity argument is passed", () => {
            const input = {
                quantity: 2
            };

            const output = {
                type: ADDED_TO_CART,
                payload: {
                    _id: undefined,
                    quantity: 2
                }
            };

            expect(addToCart(undefined, input.quantity)).toStrictEqual(output);
        });

        test("addToCart return value if all arguments are passed", () => {
            const input = {
                id: 1,
                quantity: 2
            };
            const output = {
                type: ADDED_TO_CART,
                payload: {
                    _id: 1,
                    quantity: 2
                }
            };

            expect(addToCart(input.id, input.quantity)).toStrictEqual(output);
        });
    })

    describe("removeFromCart", () => {
        test("removeFromCart smoke", () => {
            expect(typeof removeFromCart()).toBe("object");
        })

        test("removeFromCart return value if no arguments passed", () => {
            const output = {
                type: REMOVED_FROM_CART,
                payload: undefined
            };
            expect(removeFromCart(undefined)).toStrictEqual(output);
        });

        test("removeFromCart return value if all arguments are passed", () => {
            const input = 1;
            const output = {
                type: REMOVED_FROM_CART,
                payload: input
            };

            expect(removeFromCart(input)).toStrictEqual(output);
        });
    });

    describe("removeAll", () => {
        test("removeAll smoke", () => {
            expect(typeof removeAll()).toBe("object");
        })

        test("removeAll return value ", () => {
            const output = {
                type: REMOVED_ALL,
            };
            expect(removeAll()).toStrictEqual(output);
        });
    });

});

describe("cart selectors", () => {
    const {cart} = selectors;
    test("smoke", () => {
        const state = {
            cart: []
        }

        const result = cart(state);
        expect(result).toEqual([]);
    })
});

describe("cart operations", () => {

});

