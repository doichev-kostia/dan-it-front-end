import types from "./types.js";

const getCart = ( data ) => ({
    type: types.GOT_CART,
    payload: data
});

const setAmount = ( updatedCart ) => ({
    type: types.SET_AMOUNT,
    payload: updatedCart
});

const increaseAmount = ( updatedCart ) => ({
    type: types.INCREASED_AMOUNT,
    payload: updatedCart
});

const decreaseAmount = ( updatedCart ) => ({
    type: types.DECREASED_AMOUNT,
    payload: updatedCart
});

const addToCart = ( id, quantity ) => ({
    type: types.ADDED_TO_CART,
    payload: {
        _id: id,
        quantity: quantity
    }
});

const removeFromCart = ( id ) => ({
    type: types.REMOVED_FROM_CART,
    payload: id
});

const removeAll = () => ({
    type: types.REMOVED_ALL
});

export default {
    getCart,
    setAmount,
    increaseAmount,
    decreaseAmount,
    addToCart,
    removeFromCart,
    removeAll,
};