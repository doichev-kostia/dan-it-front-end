import actions from "./actions";
import types from "./types";
import reducers from "./reducers";
import selectors from "./selectors";

const {
    addToFavourites,
    removeFromFavourites,
    setFavourites
} = actions;

const {
    ADDED_TO_FAVOURITES,
    REMOVED_FROM_FAVOURITES,
    SET_FAVOURITES
} = types;

describe("reducers", () => {
    const reducer = reducers.favourites;

    test("smoke", () => {
        const result = reducer(undefined, {type: "@@INIT"});
        expect(result).toEqual([]);
    });

    test("if it returns correct state in case ADDED_TO_FAVOURITES action is passed", () => {
        const action = addToFavourites(1);
        const result = reducer([], action);
        expect(result).toEqual([action.payload]);
    });

    test("if it returns correct state in case REMOVED_FROM_FAVOURITES action is passed", () => {
        const previousState = [
            {
                _id: 1
            },
            {
                _id: 2
            }
        ];
        const action = removeFromFavourites(1);
        const result = reducer(previousState, action);
        expect(result).toEqual([{_id: 2}]);
    });

    test("if it returns correct state in case SET_FAVOURITES action is passed", () => {
        const action = setFavourites([{_id: 2}]);
        const result = reducer([], action);
        expect(result).toEqual([{_id: 2}]);
    });
});

describe("selectors", () => {
    const favourites = selectors.favourites;
    test("smoke", () => {
        const state = {
            favourites: [{_id: 1}]
        };
        const result = favourites(state);
        expect(result).toEqual(state.favourites);
    });
});

describe("actions", () => {
    describe("addToFavourites", () => {
        test("smoke", () => {
            expect(typeof addToFavourites()).toBe("object")
        });

        test("return value if no arguments is passed", () => {
            const output = {
                type: ADDED_TO_FAVOURITES,
                payload: {_id: undefined}
            };

            expect(addToFavourites(undefined)).toEqual(output);
        })

        test("return value if correct arguments is passed", () => {
            const output = {
                type: ADDED_TO_FAVOURITES,
                payload: {_id: 1}
            };
            expect(addToFavourites(1)).toEqual(output);
        })
    });

    describe("removeFromFavourites", () => {
        test("smoke", () => {
            expect(typeof removeFromFavourites()).toBe("object")
        });

        test("return value if no arguments is passed", () => {
            const output = {
                type: REMOVED_FROM_FAVOURITES,
                payload: undefined
            };

            expect(removeFromFavourites(undefined)).toEqual(output);
        })

        test("return value if correct arguments is passed", () => {
            const output = {
                type: REMOVED_FROM_FAVOURITES,
                payload: 1
            };
            expect(removeFromFavourites(1)).toEqual(output);
        })
    });

    describe("setFavourites", () => {
        test("smoke", () => {
            expect(typeof setFavourites()).toBe("object")
        });

        test("return value if no arguments is passed", () => {
            const output = {
                type: SET_FAVOURITES,
                payload: undefined
            };

            expect(setFavourites(undefined)).toEqual(output);
        })

        test("return value if correct arguments is passed", () => {
            const output = {
                type: SET_FAVOURITES,
                payload: [{_id: 1}]
            };
            expect(setFavourites([{_id: 1}])).toEqual(output);
        })
    });
});

describe("operations", () => {});
