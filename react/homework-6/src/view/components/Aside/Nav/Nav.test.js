import {createMemoryHistory} from "history";
import {render} from "@testing-library/react";
import {Router} from "react-router-dom";
import Nav from "./Nav";
import userEvent from "@testing-library/user-event";

describe("Nav", () => {
    test("smoke", () => {
        const history = createMemoryHistory();
        const {getByRole} = render(
            <Router history={history}>
                <Nav/>
            </Router>
        );
       expect(getByRole("navigation")).toBeInTheDocument();
    });

    test("if it goes to laptops page", () => {
        const history = createMemoryHistory();
        const {getByText} = render(
            <Router history={history}>
                <Nav/>
            </Router>
        );
        userEvent.click(getByText(/ноутбук/i));
        expect(history.location.pathname).toMatch(/laptop/i);
    });

    test("if it goes to pc page", () => {
        const history = createMemoryHistory();
        const {getByText} = render(
            <Router history={history}>
                <Nav/>
            </Router>
        );
        userEvent.click(getByText(/компьютер/i));
        expect(history.location.pathname).toMatch(/pc/i);
    });

    test("if it goes to cell-phones page", () => {
        const history = createMemoryHistory();
        const {getByText} = render(
            <Router history={history}>
                <Nav/>
            </Router>
        );
        userEvent.click(getByText(/смартфон/i));
        expect(history.location.pathname).toMatch(/cell-phones/i);
    });

    test("if it goes to tv-and-electronics page", () => {
        const history = createMemoryHistory();
        const {getByText} = render(
            <Router history={history}>
                <Nav/>
            </Router>
        );
        userEvent.click(getByText(/электроника/i));
        expect(history.location.pathname).toMatch(/tv-and-electronics/i);
    });

    test("if it goes to gamer-goods page", () => {
        const history = createMemoryHistory();
        const {getByText} = render(
            <Router history={history}>
                <Nav/>
            </Router>
        );
        userEvent.click(getByText(/гейм/i));
        expect(history.location.pathname).toMatch(/gamer-goods/i);
    });

    test("if it goes to accessories page", () => {
        const history = createMemoryHistory();
        const {getByText} = render(
            <Router history={history}>
                <Nav/>
            </Router>
        );
        userEvent.click(getByText(/аксессуар/i));
        expect(history.location.pathname).toMatch(/accessories/i);
    });
})