import React from "react";
import "./Nav.scss"
import { NavLink } from "react-router-dom";

export default function Nav (){
        return (
            <nav className="nav">
                <ul className="nav__list">
                    <li className="nav__item">
                        <NavLink activeClassName="nav__link--active" className="nav__link link" to="/laptops">Ноутбуки</NavLink>
                    </li>
                    <li className="nav__item">
                        <NavLink activeClassName="nav__link--active" className="nav__link link" to="/pc">Компьютеры</NavLink>
                    </li>
                    <li className="nav__item">
                        <NavLink activeClassName="nav__link--active" className="nav__link link" to="/cell-phones">Смартфоны</NavLink>
                    </li>
                    <li className="nav__item">
                        <NavLink activeClassName="nav__link--active" className="nav__link link" to="/tv-and-electronics">ТВ и электроника</NavLink>
                    </li>
                    <li className="nav__item">
                        <NavLink activeClassName="nav__link--active" className="nav__link link" to="/gamer-goods">Товары для геймеров</NavLink>
                    </li>
                    <li className="nav__item">
                        <NavLink activeClassName="nav__link--active" className="nav__link link" to="/accessories">Аксессуары</NavLink>
                    </li>
                </ul>
            </nav>
        );
}
