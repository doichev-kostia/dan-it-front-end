import React from "react";
import Nav from "./Nav/Nav.jsx";

export default function Aside(props) {
    return (
        <aside data-testid="aside" className="aside">
            <Nav/>
        </aside>
    );
}
