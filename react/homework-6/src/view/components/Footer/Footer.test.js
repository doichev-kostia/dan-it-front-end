import {render, screen} from "@testing-library/react";
import Footer from "./Footer";

const {getByText} = screen;

describe("Footer", () => {
    test("Smoke", () => {
        render(<Footer/>)
    });

    test("If textContent displays properly", () => {
        render(<Footer/>)
        expect(getByText(/copyright/i)).toBeInTheDocument();
    });
});
