import React from "react";
import "./Header.scss";
import {
    userIcon,
    cartHeaderIcon,
    starIcon
} from "../../utilities/Icons/iconDetails.js";
import {
    Link,
    NavLink,
} from "react-router-dom";

export default function Header() {
    return (
        <header data-testid="header" className="header">
            <div className="wrapper header-layout">
                <Link data-testid="logo" to="/" className="header__logo">
                    <p className="header__logo-text">Computer Geek</p>
                </Link>
                <ul className="header-actions">
                    <li className="header-actions__item">
                        <NavLink
                            data-testid="user-icon"
                            to="/user"
                            activeClassName="header-actions__link--active"
                            className="header-actions__authorization header-actions__link">
                            {userIcon}
                        </NavLink>
                    </li>
                    <li className="header-actions__item">
                        <NavLink
                            to="/cart"
                            data-testid="cart-icon"
                            activeClassName="header-actions__link--active"
                            className="header-actions__cart header-actions__link">
                            {cartHeaderIcon}
                        </NavLink>
                    </li>
                    <li className="header-actions__item">
                        <NavLink
                            to="/favourites"
                            data-testid="star-icon"
                            activeClassName="header-actions__link--active"
                            className="header-actions__favourites header-actions__link">
                            {starIcon}
                        </NavLink>
                    </li>
                </ul>
            </div>
        </header>
    );

}
