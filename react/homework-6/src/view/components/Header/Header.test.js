import {render} from "@testing-library/react";
import Header from "./Header";
import {Router} from "react-router-dom";
import {createMemoryHistory} from "history";
import userEvent from "@testing-library/user-event";

describe("Header", () => {
    test("smoke", () => {
        const history = createMemoryHistory();
        render(
            <Router history={history}>
                <Header/>
            </Router>);
    });

    test("if component renders properly", () => {
        const history = createMemoryHistory();
        const {getByTestId} = render(
            <Router history={history}>
                <Header/>
            </Router>);
        expect(getByTestId("header")).toBeInTheDocument();
    });

    test("if it goes to home page after clicking on logo", () => {
        const history = createMemoryHistory();
        const {getByTestId} = render(
            <Router history={history}>
                <Header/>
            </Router>);
        const logo = getByTestId("logo");
        userEvent.click(logo);
        expect(history.location.pathname).toMatch("/")
    });

    test("if it goes to user page after clicking on user icon", () => {
        const history = createMemoryHistory();
        const {getByTestId} = render(
            <Router history={history}>
                <Header/>
            </Router>);
        const icon = getByTestId("user-icon");
        userEvent.click(icon);
        expect(history.location.pathname).toMatch("/user")
    });

    test("if it goes to cart page after clicking on cart icon", () => {
        const history = createMemoryHistory();
        const {getByTestId} = render(
            <Router history={history}>
                <Header/>
            </Router>);
        const icon = getByTestId("cart-icon");
        userEvent.click(icon);
        expect(history.location.pathname).toMatch("/cart");
    });

    test("if it goes to favourites page after clicking on star icon", () => {
        const history = createMemoryHistory();
        const {getByTestId} = render(
            <Router history={history}>
                <Header/>
            </Router>);
        const icon = getByTestId("star-icon");
        userEvent.click(icon);
        expect(history.location.pathname).toMatch("/favourites");
    });
});