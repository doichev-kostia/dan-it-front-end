import userEvent from "@testing-library/user-event";
import {isModalOpenOperations} from "../../../redux/features/isModalOpen";
import store from "../../../redux/store/store";
import renderWithRedux from "../../../utils/renderWithRedux";

import Modal, {handleModalClosing} from "./Modal";

const modalState = {
    initialState: true,
    store
}

const props = {
    header: "Test header",
    text: "test text",
    id: "test id",
    actions: [<button>Test</button>],
    children: <p>Child</p>
}

describe("Modal", () => {
    beforeEach(() => {
        store.dispatch(isModalOpenOperations.openModal())
    });


    test("Smoke", () => {
        renderWithRedux(<Modal header={props.header} closeButton/>, modalState);
    });

    test("if it doesn't render in case state is false", () => {
        store.dispatch(isModalOpenOperations.closeModal());
        const {queryByTestId} = renderWithRedux(<Modal data-testid="modal" header={props.header}
                                                       closeButton/>, modalState);
        expect(queryByTestId("modal")).toBeNull();
    });

    test("if it renders correct heading prop", () => {
        const {getByRole} = renderWithRedux(<Modal header={props.header} closeButton/>, modalState);
        expect(getByRole("heading")).toBeInTheDocument();
    });

    test("if it renders cross button", () => {
        const {getByRole} = renderWithRedux(<Modal header={props.header} closeButton/>, modalState);
        expect(getByRole("button")).toBeInTheDocument();
    });

    test("if it renders default classNames ", () => {
        const {getByTestId} = renderWithRedux(<Modal data-testid="modal" header={props.header}
                                                     closeButton/>, modalState);
        expect(getByTestId("modal")).toHaveClass("modal");
    });

    test("if it renders text prop correctly", () => {
        const {getByText} = renderWithRedux(<Modal
            data-testid="modal"
            header={props.header}
            text={props.text}
            closeButton/>, modalState);
        expect(getByText(props.text)).toBeInTheDocument();
    });

    test("if it renders id prop correctly", () => {
        const {getByTestId} = renderWithRedux(<Modal
            data-testid="modal"
            header={props.header}
            id={props.id}
            closeButton/>, modalState);
        expect(getByTestId("modal")).toHaveAttribute("data-modal-id", props.id);
    });

    test("if it renders classNames prop correctly", () => {
        const classNames = {
            modal: "modal-test",
            modalBody: "modal-body",
            crossButton: "cross-btn modal-body__cross-btn",
            header: "modal-body__header",
            headerWrapper: "modal-body__header-wrapper",
            text: "modal-body__text",
            buttons: "modal-body__buttons"
        }
        const {getByTestId} = renderWithRedux(<Modal
            data-testid="modal"
            header={props.header}
            classNames={classNames}
            closeButton/>, modalState);
        expect(getByTestId("modal")).toHaveClass(classNames.modal);
    });

    test("If it renders correct actions prop", () => {
        const {getByRole} = renderWithRedux(<Modal
            data-testid="modal"
            header={props.header}
            actions={props.actions}
            closeButton={false}/>, modalState);
        expect(getByRole("button")).toBeInTheDocument();
    });

    test("if it renders correct children prop", () => {
        const {getByText} = renderWithRedux(<Modal
            data-testid="modal"
            header={props.header}
            children={props.children}
            closeButton={false}/>, modalState);
        const {children: ModalChild} = props;
        expect(getByText(ModalChild.props.children)).toBeInTheDocument();
    });

    test("if modal closes on cross button click", () => {
        const {getByRole, queryByTestId} =renderWithRedux(<Modal
            data-testid="modal"
            header={props.header}
            closeButton/>, modalState);

        const crossBtn = getByRole("button");
        expect(queryByTestId("modal")).toBeInTheDocument();
        userEvent.click(crossBtn);
        expect(queryByTestId("modal")).toBeNull();
    })

    test("if modal closes on modal wrapper click", () => {
        const {queryByTestId} =renderWithRedux(<Modal
            data-testid="modal"
            header={props.header}
            closeButton/>, modalState);

        const modalWrapper = queryByTestId("modal");
        expect(queryByTestId("modal")).toBeInTheDocument();
        userEvent.click(modalWrapper);
        expect(queryByTestId("modal")).toBeNull();
    });

    test("if isOpenModal state changes when modal closes", () => {
        const {getByRole, queryByTestId} =renderWithRedux(<Modal
            data-testid="modal"
            header={props.header}
            closeButton/>, modalState);

        let isModalOpen = store.getState().isModalOpen

        const crossBtn = getByRole("button");
        expect(isModalOpen).toBeTruthy();
        userEvent.click(crossBtn);
        isModalOpen = store.getState().isModalOpen
        expect(isModalOpen).toBeFalsy();
    });

})
