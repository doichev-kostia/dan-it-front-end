import renderWithRedux from "../../../utils/renderWithRedux";
import ProductCard from "./ProductCard";
import {screen} from "@testing-library/react";
import store from "../../../redux/store/store";
import {Router} from "react-router-dom";
import {createMemoryHistory} from "history";
import {goodsActions} from "../../../redux/features/goods";
import React from "react";
import {convertNumberToReadableString} from "../../utilities/convertNumberToReadableString/convertNumberToReadableString";
import userEvent from "@testing-library/user-event";

const DATA = [{
    _id: "13213",
    title: "Ноутбук Apple MacBook Pro 16 512GB 2019",
    price: {
        UAH: {
            value: 100000,
            symbol: "₴",
            oldPrice: 120000
        },
        USD: {
            value: 3500,
            symbol: "$",
            oldPrice: 3600
        },
    },
    image: "./img/goods/macbook-pro-16.jpeg",
    characteristic: "Some text",
    colors: [
        {
            title: "Black",
            rgb: "rgb(0,0,0)",
            hex: "#000000"
        },
        {
            title: "White",
            rgb: "rgb(255,255,255)",
            hex: "#FFFFFF"
        }
    ]
}];

const [card] = DATA

describe("Product Card", () => {
    let goodsList;
    beforeEach(() => {
        const action = goodsActions.getData(DATA);
        store.dispatch(action);
        const {goods, cart, favourites} = store.getState();
        goodsList = goods.map(card => {
            const isOnCart = cart.some(object => object._id === card._id);
            const isFavourite = favourites.some(object => object._id === card._id);

            return <ProductCard
                key={card._id}
                card={card}
                isOnCart={isOnCart}
                isFavourite={isFavourite}
                classNames={{listItem: "test"}}/>;
        });
    })
    test("smoke", () => {
        const history = createMemoryHistory();
        renderWithRedux(
            <Router history={history}>
                <ul>
                    {goodsList}
                </ul>
            </Router>,
            {initialState: [], store});
    });

    test("if it renders title properly", () => {
        const history = createMemoryHistory();
        const {getByText} = renderWithRedux(
            <Router history={history}>
                <ul>
                    {goodsList}
                </ul>
            </Router>,
            {initialState: [], store});
        expect(getByText(card.title)).toBeInTheDocument()
    });

    test("if it renders price properly", () => {
        const history = createMemoryHistory();
        const {getByText} = renderWithRedux(
            <Router history={history}>
                <ul>
                    {goodsList}
                </ul>
            </Router>,
            {initialState: [], store});
        const price = convertNumberToReadableString(card.price.UAH.value)
        expect(getByText(price)).toBeInTheDocument();
    });

    test("if it renders image link properly", () => {
        const history = createMemoryHistory();
        const {getByRole} = renderWithRedux(
            <Router history={history}>
                <ul>
                    {goodsList}
                </ul>
            </Router>,
            {initialState: [], store});
        expect(getByRole("img")).toHaveAttribute("src", card.image);
    });

    test("if it goes to the own page when the link is clicked", () => {
        const history = createMemoryHistory();
        const {getByTestId} = renderWithRedux(
            <Router history={history}>
                <ul>
                    {goodsList}
                </ul>
            </Router>,
            {initialState: [], store});
        const link = getByTestId("image-link");
        userEvent.click(link);
        expect(history.location.pathname).toMatch(`/goods/${card._id}`)
    })

})