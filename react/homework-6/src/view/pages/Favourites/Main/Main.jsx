import React from "react";
import FavouriteSection from "./FavouriteSection/FavouriteSection.jsx";

function Main() {
    return (
        <main data-testid="main-content" className="main-content">
            <h1 className="main-content__heading">Избранное</h1>
            <FavouriteSection />
        </main>
    );
}

export default Main;