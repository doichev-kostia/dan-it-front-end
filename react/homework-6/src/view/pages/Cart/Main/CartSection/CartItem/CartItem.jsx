import React, { useEffect, useRef, useState } from "react";
import PropTypes from "prop-types";
import Button from "../../../../../components/Button/Button.jsx";
import "./CartItem.scss";
import {
    plusIcon,
    minusIcon,
    disabledMinusIcon
} from "../../../../../utilities/Icons/iconDetails.js";
import Modal, { generateModalData } from "../../../../../components/Modal/Modal.jsx";
import { modalIDs } from "../../../../../components/Modal/modalParameters.js";
import { useDispatch, useSelector } from "react-redux";
import {
    cartOperations,
} from "../../../../../../redux/features/cart";
import {
    isModalOpenOperations,
    isModalOpenSelectors
} from "../../../../../../redux/features/isModalOpen";
import { uniqueKey } from "../../../../../utilities/numberGenerators/numberGenerators.js";
import { Link } from "react-router-dom";
import { convertNumberToReadableString } from "../../../../../utilities/convertNumberToReadableString/convertNumberToReadableString.js";

CartItem.propTypes = {
    quantity: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    card: PropTypes.shape({
        title: PropTypes.string,
        price: PropTypes.object,
        image: PropTypes.string,
        id: PropTypes.string,
        colors: PropTypes.arrayOf(PropTypes.object)
    }),
};

/**
 * @todo
 * create currency state in Redux
 * */

function CartItem( props ) {
    const dispatch = useDispatch();
    const isOpenModal = useSelector(isModalOpenSelectors.isModalOpen);
    const [openModalID, setOpenModalID] = useState(null);
    const [quantity, setQuantity] = useState(props.quantity);
    const [errors, setErrors] = useState({});
    const [touched, setTouched] = useState({
        quantity: false
    });
    const quantityInput = useRef(null);

    // const chosenCurrencyValue = price[reduxCurrency].value;
    // const chosenCurrencySymbol = price[reduxCurrency].symbol;
    // const chosenCurrencyOldPrice = price[reduxCurrency].oldPrice


    let {card} = props;
    const {title, price, image, _id} = card;

    useEffect(() => {
        if ( !isOpenModal ) {
            setOpenModalID(null);
        }
    }, [openModalID, isOpenModal]);

    let chosenModalData;

    if ( openModalID ) {
        chosenModalData = generateModalData(openModalID);
    }

    return (
        <div className="cart-tile">
            <div className="cart-tile__inner">
                <Link to={ `/goods/${_id}` } className="cart-tile__picture-link">
                    <picture className="cart-tile__picture">
                        <img src={ image } alt={ title }/>
                    </picture>
                </Link>
                <div className="cart-tile__details">
                    <p className="cart-tile__title">{ title }</p>
                    <div className="cart-tile__counter cart-counter">
                        <Button
                            disabled={ quantity === 1 }
                            children={ quantity === 1 ? disabledMinusIcon : minusIcon }
                            onClick={ () => removeItemFromCart(_id, dispatch, setQuantity, quantityInput.current, setErrors) }
                            className="cart-counter__btn minus-btn"/>
                        <input
                            ref={quantityInput}
                            type="number"
                            name="quantity"
                            className="cart-counter__quantity"
                            value={ quantity }
                            onFocus={ ( e ) =>
                                setTouched({
                                    ...touched,
                                    [e.target.name]: true
                                }) }
                            onChange={ ( e ) =>
                                handleInputChange(e, setQuantity) }
                            onBlur={ ( e ) => handleInputBlur(e, _id, dispatch, setErrors) }/>
                        <Button
                            onClick={ () => addToCart(_id, dispatch, setQuantity, quantityInput.current, setErrors) }
                            children={ plusIcon }
                            className="cart-counter__btn plus-btn"/>
                        { touched.quantity && errors.quantity &&
                        <p className="cart-counter__alert">{ errors.quantity }</p> }
                    </div>
                </div>
                <div className="cart-tile__prices">
                    <p className="cart-tile__price">
                          <span
                              className="cart-tile__price-value">{ convertNumberToReadableString(price.UAH.value) }</span>
                        <span
                            className="cart-tile__price-currency">{ price.UAH.symbol }</span>
                        {/*<span */ }
                        {/*    className="cart-tile__price--old">{chosenCurrencyOldPrice} <small>{ chosenCurrencySymbol }</small>   </span>*/ }
                        {/*<span*/ }
                        {/*     className="cart-tile__price-value">{ chosenCurrencyValue }</span>*/ }
                        {/*<span*/ }
                        {/*    className="cart-tile__price-currency">{ chosenCurrencySymbol }</span>*/ }
                    </p>
                </div>
                <Button
                    text="X"
                    className="cart-tile__cross-btn"
                    onClick={ ( e ) => handleCrossBtnClick(dispatch, setOpenModalID) }/>
            </div>
            { openModalID
            &&
            <Modal
                { ...chosenModalData }
                actions={ declareCartModalActionButtons(_id, dispatch) }/> }
        </div>
    );
}

function handleInputChange( event, setQuantity ) {
    const {target} = event;
    let {value} = target;
    value = Number(value);
    setQuantity(value);
}

function handleInputBlur( event, id, dispatch, setErrors ) {
    const value = quantityInputValidation(event.target, setErrors)
    if ( value ) {
        dispatch(cartOperations.setAmount(id, value));
    }

}

function removeItemFromCart( id, dispatch, setQuantity, input, setErrors ) {
    const value = quantityInputValidation(input, setErrors)
    if ( value ) {
        dispatch(cartOperations.decreaseAmount(id));
        setQuantity(prevValue => prevValue -= 1)
    }
}

function addToCart( id, dispatch, setQuantity, input, setErrors ) {
    const value = quantityInputValidation(input, setErrors)
    if ( value ) {
        dispatch(cartOperations.increaseAmount(id));
        setQuantity(prevValue => prevValue += 1)
    }
}

function handleCrossBtnClick( dispatch, setOpenModalID ) {
    dispatch(isModalOpenOperations.openModal());
    setOpenModalID(modalIDs.deleteFromCart);
}

function quantityInputValidation (element, setErrors) {
    let {value, name} = element;
    value = Number(value);
    if ( value < 1 || !(Number.isInteger(value)) ) {
        setErrors(prevState => {
            return {
                ...prevState,
                [name]: "Неправильно указано количество"
            };
        });
        return null;
    } else {
        setErrors(prevState => {
            return {
                ...prevState,
                [name]: ""
            };
        });
        return value
    }
}




function declareCartModalActionButtons( productID, dispatch ) {
    return [
        <Button
            key={ uniqueKey() }
            text="OK"
            backgroundColor="#23A77BFF"
            className="modal-body__btn modal__submit-btn"
            onClick={ () => dispatch(cartOperations.removeFromCart(productID))
            }
        />,
        <Button
            key={ uniqueKey() }
            text="Отменить"
            backgroundColor="#23A77BFF"
            className="modal-body__btn modal__cancel-btn"
        />
    ];
}



export default CartItem;