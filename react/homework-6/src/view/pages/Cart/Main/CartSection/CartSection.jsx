import React, { useEffect, useState } from "react";
import CartList from "./CartList/CartList.jsx";
import "./CartSection.scss";
import Button from "../../../../components/Button/Button.jsx";
import { useDispatch, useSelector } from "react-redux";
import {
    isModalOpenOperations,
    isModalOpenSelectors
} from "../../../../../redux/features/isModalOpen";
import Modal from "../../../../components/Modal/Modal.jsx";
import OrderForm from "./OrderForm/OrderForm.jsx";
import { cartSelectors } from "../../../../../redux/features/cart";
import { goodsSelectors } from "../../../../../redux/features/goods";
import { convertNumberToReadableString } from "../../../../utilities/convertNumberToReadableString/convertNumberToReadableString.js";

function CartSection() {
    const dispatch = useDispatch();
    const cart = useSelector(cartSelectors.cart);
    const goods = useSelector(goodsSelectors.goods);
    const isModalOpen = useSelector(isModalOpenSelectors.isModalOpen);
    const [isLocalModalOpen, setLocalModalOpen] = useState(false);

    useEffect(() => {
        if ( !isModalOpen ) {
            setLocalModalOpen(false);
        }
    }, [isLocalModalOpen, isModalOpen]);

    return (
        <section className="cart">
            <CartList/>
            <div className="cart-trade">
                { cart.length > 0 &&
                <>
                    <p className="cart-trade__sum">Сумма: { convertNumberToReadableString(calculateCartSum(cart, goods)) }₴</p>
                    <Button
                        onClick={ () => handleButtonClick(dispatch, setLocalModalOpen) }
                        text="Оформить заказ" className="cart-trade__buy-btn"/>
                </>
                }
            </div>
            { isLocalModalOpen
            &&
            <Modal
                classNames={ {
                    modal: "modal",
                    modalBody: "modal-body modal-body--green",
                    crossButton: "cross-btn modal-body__cross-btn",
                    header: "modal-body__header",
                    headerWrapper: "modal-body__header-wrapper"
                } }
                header="Оформить заказ"
                closeButton={ true }
                children={ <OrderForm/> }/> }
        </section>
    );
}

function handleButtonClick( dispatch, setLocalModalOpen ) {
    dispatch(isModalOpenOperations.openModal());
    setLocalModalOpen(true);
}

function calculateCartSum( cart, goods ) {
    const cartObjects = goods.filter(product => {
        return cart.some(object => product._id === object._id);
    });

    let result = 0;

    cartObjects.forEach(object => {
        const cartItem = cart.find(item => item._id === object._id);
        const sum = Number((object.price.UAH.value) * cartItem.quantity);
        result += sum;
    });

    return result;
}

export default CartSection;