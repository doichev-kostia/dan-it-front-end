import * as Yup from "yup";

export const formValidationSchema = Yup.object().shape({
    firstName: Yup
        .string()
        .max(20, "Sorry, this name is too long")
        .required("Fill this field, please"),
    lastName: Yup
        .string()
        .max(25, "Sorry, this last name is too long")
        .required("Fill this field, please"),
    age: Yup
        .number()
        .min(1, "Sorry, that's not a valid age.It should be in range of 1" +
            " and 105")
        .max(105, "Sorry, that's not a valid age.It should be in range of 1" +
            " and 105")
        .required("Fill this field, please"),
    address: Yup.string().required("Fill this field, please"),
    email: Yup.string().email("This field should contain a valid email").required("Fill this field, please"),
})
