import Main from "./Main";
import {render} from "@testing-library/react";

jest.mock("./CartSection/CartSection", () => () => (<p data-testid="test-child">Hello, world</p>))

describe("Cart main", () => {
    test("smoke", () => {
        render(<Main/>);
    });

    test("if it renders main properly", () => {
        const {getByTestId} = render(<Main/>);
        expect(getByTestId("main-content")).toBeInTheDocument();
    });

    test("if it renders main heading", () => {
        const {getByRole} = render(<Main/>);
        expect(getByRole("heading")).toBeInTheDocument();
    });

    test("if it renders child component", () => {
        const {getByTestId} = render(<Main/>);
        expect(getByTestId("test-child")).toBeInTheDocument();
    })
});