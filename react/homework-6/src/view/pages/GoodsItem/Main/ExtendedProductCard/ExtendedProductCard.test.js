import renderWithRedux from "../../../../../utils/renderWithRedux";
import ExtendedProductCard from "./ExtendedProductCard";
import {convertNumberToReadableString} from "../../../../utilities/convertNumberToReadableString/convertNumberToReadableString";

const item = {
    _id: "13213",
    title: "Ноутбук Apple MacBook Pro 16 512GB 2019",
    price: {
        UAH: {
            value: 100000,
            symbol: "₴",
            oldPrice: 120000
        },
        USD: {
            value: 3500,
            symbol: "$",
            oldPrice: 3600
        },
    },
    image: "./img/goods/macbook-pro-16.jpeg",
    characteristic: "Some text",
    colors: [
        {
            title: "Black",
            rgb: "rgb(0,0,0)",
            hex: "#000000"
        },
        {
            title: "White",
            rgb: "rgb(255,255,255)",
            hex: "#FFFFFF"
        }
    ]
}

describe("ExtendedProductCart", () => {
    test("smoke", () => {
        renderWithRedux(<ExtendedProductCard chosenItem={item} isFavourite/>)
    });

    test("if card rendered properly", () => {
        const {getByTestId} = renderWithRedux(<ExtendedProductCard chosenItem={item} isFavourite/>);
        expect(getByTestId("product-card")).toBeInTheDocument();
    });

    test("if image rendered", () => {
        const {getByRole} = renderWithRedux(<ExtendedProductCard chosenItem={item} isFavourite/>);
        expect(getByRole("img")).toHaveAttribute("src", `../${item.image}`);
        expect(getByRole("img")).toHaveAttribute("alt", item.title + " image");
    });

    test("if price rendered", () => {
        const {getByTestId} = renderWithRedux(<ExtendedProductCard chosenItem={item} isFavourite/>);
        expect(getByTestId("price")).toBeInTheDocument();
    });

    test("if characteristic rendered", () => {
        const {getByText} = renderWithRedux(<ExtendedProductCard chosenItem={item} isFavourite/>);
        expect(getByText(item.characteristic)).toBeInTheDocument();
    });

    test("if isFavourite button rendered", () => {
        const {getByTestId} = renderWithRedux(<ExtendedProductCard chosenItem={item} isFavourite/>);
        expect(getByTestId("favourite-btn")).toBeInTheDocument();
    })
})