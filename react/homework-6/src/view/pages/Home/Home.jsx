import React from "react";
import Main from "./Main/Main.jsx";
import Aside from "../../components/Aside/Aside.jsx";

export default function Home() {

    return (
        <React.Fragment>
            <Aside/>
            <Main/>
        </React.Fragment>
    );
}
