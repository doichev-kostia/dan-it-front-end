import {render} from "@testing-library/react";
import Goods from "./Goods";

jest.mock("./GoodsList/GoodsList", () => () => <p data-testid="test-child">Hello, world</p>)

describe("Goods", () => {
    test("smoke", () => {
        render(<Goods/>)
    });

    test("if it renders correctly", () => {
        const {getByTestId} = render(<Goods/>);
        expect(getByTestId("goods")).toBeInTheDocument();
    });

    test("if it renders children", () => {
        const {getByTestId} = render(<Goods/>);
        expect(getByTestId("test-child")).toBeInTheDocument();
    })
});