import renderWithRedux from "../../../../../../utils/renderWithRedux";
import GoodsList from "./GoodsList";
import store from "../../../../../../redux/store/store";
import {goodsActions} from "../../../../../../redux/features/goods";
import {Router} from "react-router-dom";
import {createMemoryHistory} from "history";

        jest.mock("../../../../../components/ProductCard/ProductCard", () => () => (<p>Hello, world</p>) )
describe("GoodsList", () => {
    test("smoke", () => {
        renderWithRedux(<GoodsList/>, {initialState: undefined, store});
    });

    test("if it renders alert", () => {
        const {getByText} = renderWithRedux(<GoodsList/>, {initialState: undefined, store});
        expect(getByText(/товаров/i)).toBeInTheDocument();
    })

    test("if it renders goods", () => {
        const action = goodsActions.getData([
            {title: "test"}
        ]);
        store.dispatch(action);
        const {getByTestId, debug} = renderWithRedux(
                <GoodsList/>
            , {initialState: undefined, store});
        expect(getByTestId("goods-list")).toBeInTheDocument();
    })
})