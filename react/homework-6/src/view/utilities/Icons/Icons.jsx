import * as AllIcons from "./allIcons.js";
import PropTypes from "prop-types";

function Icons({name, fillColor, className, ...restProps}) {
    const currentIcon = AllIcons[name];

    if (!currentIcon) {
        return null;
    }


    return (
            currentIcon(className, fillColor)
    );
}

Icons.propTypes = {
    name: PropTypes.string.isRequired,
    fillColor: PropTypes.string,
    className: PropTypes.string
};

Icons.defaultProps = {
    fillColor: "",
};


export default Icons;


