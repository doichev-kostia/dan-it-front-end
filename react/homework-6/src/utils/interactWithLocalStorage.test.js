import {putDataInLocalStorage, getDataFromLocalStorage} from "./interactWithLocalStorage";

describe("interactWithLocalStorage", () => {
    describe("putDataInLocalStorage", () => {
        jest.spyOn(window.localStorage.__proto__, 'setItem');
        window.localStorage.__proto__.setItem = jest.fn();
        window.localStorage.__proto__.getItem = jest.fn();

        test("smoke", () => {
            putDataInLocalStorage("test", "test");
        });

        test("to check if setItem func works", () => {
            putDataInLocalStorage("test", "test");
            expect(localStorage.setItem).toBeCalled();
        });

        test("if function throws an exception in case data argument is undefined", () => {
            const funcWrapper = () => {
                putDataInLocalStorage("test", undefined);
            }
            expect(funcWrapper).toThrow();
        });
    });

    describe("getDataFromLocalStorage", () => {
        jest.spyOn(window.localStorage.__proto__, 'setItem');
        window.localStorage.__proto__.setItem = jest.fn();
        window.localStorage.__proto__.getItem = jest.fn();
        jest.spyOn(JSON, "parse")
        JSON.parse = jest.fn();

        test("smoke", () => {
            getDataFromLocalStorage("test")
        });

        test("if function throws an exception in case keyName argument is undefined", () => {
            const funcWrapper = () => {
                getDataFromLocalStorage(undefined);
            };

            expect(funcWrapper).toThrow();
        });
    });
})
