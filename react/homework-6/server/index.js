const express = require("express");
const bodyParser = require("body-parser")
const MongoClient = require("mongodb").MongoClient;
const db = require("./config/db.js");

const PORT = process.env.PORT || 8085;

const app = express();
let dbClient;

app.use(express.static(__dirname + "/public/"));
app.use(express.urlencoded())
app.use(bodyParser.json());

const mongoClient = new MongoClient(db.url, { useUnifiedTopology: true });

mongoClient.connect( async ( err, client ) => {
    if ( err ) return console.log(err);
    dbClient = client
    app.listen(PORT, () => {
        console.log("server has been started...");
    });
    require("./routes")(app, client)
});

process.on("SIGINT", () => {
    dbClient.close();
    process.exit();
});