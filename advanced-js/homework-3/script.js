class Employee {

    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name
    }

    set name(value) {
        this._name = value;
    }

    get age() {
        return this._age
    }

    set age(value) {
        this._age = value;
    }

    get salary() {
        return this._salary
    }

    set salary(value) {
        this._salary = value;
    }

}

class Programmer extends Employee{
    constructor(name, age, salary, lang) {
        super(name, age, salary)
        this.lang = lang;
    }

    get salary() {
        return this._salary * 3
    }
}

const juniorDev = new Programmer("John", 22, 600, ["JavaScript"]);
const middleDev = new Programmer("Lucas", 26, 1500, ["PhP", "JavaScript", "SQL"]);
const seniorDev = new Programmer("Brad", 28, 2500, ["PhP", "JavaScript", "SQL", "C#", "Java"]);

console.log("%c Junior developer:","color:green;", juniorDev, `the salary is ${juniorDev.salary}`);
console.log("%c Middle developer:","color: yellow;", middleDev, `the salary is ${middleDev.salary}`);
console.log("%c Senior developer:", "color:red", seniorDev, `the salary is ${seniorDev.salary}`);