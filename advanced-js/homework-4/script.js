let url = "https://ajax.test-danit.com/api/swapi/films";
let container = document.querySelector("#container");

function addElementsOnPage(parent, position, elements) {
    parent.insertAdjacentHTML(position, elements);
}

function requestInfo(url) {
    return fetch(url)
        .then(response => response.json())
}

function postFilmCharacters(films) {
    const filmListItem = [...document.querySelectorAll(".films-list__item")];
    films.forEach((film, index) => {
        const currentFilm = filmListItem[index];
        const loader = [...currentFilm.children].find(item => item.classList.contains("loader"));
        const charactersUrls = film.characters;
        const charactersRequests = charactersUrls.map(url => requestInfo(url));

        Promise.all(charactersRequests)
            .then(data => {
                currentFilm.removeChild(loader)
                let names = data.map(character => `<li>${character.name} </li>`)
                let namesList = `<ul> ${names.join("")} </ul>`
                addElementsOnPage(currentFilm, "beforeend", namesList)
            })
    })
}

function postFilmInfo(parent, url) {
    requestInfo(url)
        .then(films => {
            let filmInfo = films.map(film => {
                return `<li class="films-list__item">
                               <span>Episode ${film.episodeId}</span>
                               <span> "${film.name}" </span>
                               <span> ${film.openingCrawl}</span>
                               <div class="loader"></div>
                            </li>`
            });
            let filmsList = `<ul class="films-list">${filmInfo.join("")}</ul>`;

            addElementsOnPage(parent, "afterbegin", filmsList);
            postFilmCharacters(films);
        })
}


postFilmInfo(container, url)








