const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];


const root = document.querySelector("#root");

function undefinedElement (obj){
    const {author, name, price} = obj;
    if (!author){
        return "author"
    }else if (!name){
        return  "name"
    } else if (!price){
        return "price"
    }
}

function insertBooksInHtml(objects) {
    return objects.map(item => {
        let result = []

        for (let [key, value] of Object.entries(item)) {
            result.push(`${key[0].toUpperCase() + key.slice(1)} : ${value}`);
        }

        const [author, name, price] = result;

        return `<li>
            ${author} <br>
            ${name} <br>
            ${price} <br>
            </li>`;
    });
}
let objects;
function objectsValidation(arrOfObj) {
    let counter = 0;
    let errors = [];
    objects = arrOfObj.filter(item => {
        const {author, name, price} = item;
        counter++
        if (!author || !name || !price) {
            errors.push(`Error, there is no ${undefinedElement(item)} in ${JSON.stringify(item)}`);
        }else {
            return item;
        }
    })
    if (counter === arrOfObj.length) {
        throw new Error(`${errors.join("\n")}`);
    }
}

try{

objectsValidation(books)

}catch (err){
    console.error(err.message);
}

root.insertAdjacentHTML("afterbegin",`<ul>${insertBooksInHtml(objects).join("")}</ul>`);
