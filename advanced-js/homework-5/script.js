const container = document.querySelector("#container");
const figureIPBtn = document.createElement("button");
const figureIPURL = "https://api.ipify.org/?format=json"

figureIPBtn.classList.add("button");
figureIPBtn.textContent = "Вычислить по IP";



async function figureIP(){
    const response = await fetch(figureIPURL);
    const data = await response.json()
    return data.ip
}

async function findLocationByIp(IP) {
    const response = await fetch(`http://ip-api.com/json/${IP}?fields=status,message,continent,country,regionName,city,district,query`);
    return await response.json();
}

function showElements (obj){
    for (let property in obj){
        if (obj[property] === ""){
            obj[property] = "Не найдено"
        }
    }
    const {continent, country, regionName, city, district} = obj;
    container.insertAdjacentHTML("beforeend",
        `<ul class="container__list">
                <li>Континент: ${continent}</li>
                <li>Страна: ${country}</li>
                <li>Регион: ${regionName}</li>
                <li>Район: ${city}</li>
                <li>Регион города: ${district}</li>
               </ul>`)
}

figureIPBtn.addEventListener("click", async event=>{
    const location = await findLocationByIp(await figureIP());
    showElements(location)
})

container.append(figureIPBtn)